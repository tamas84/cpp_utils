#pragma once

#include <numbers>


namespace utils
{

class Angle;
constexpr Angle operator ""_deg(long double deg) noexcept;
constexpr Angle operator ""_rad(long double rad) noexcept;

constexpr Angle operator+(const Angle& lhs, const Angle& rhs) noexcept;
constexpr Angle operator-(const Angle& lhs, const Angle& rhs) noexcept;

class Angle
{
public:
   explicit constexpr Angle() noexcept
      : m_angleRad{ 0.f }
   {}

   explicit constexpr Angle(float angleRad) noexcept
      : m_angleRad{ angleRad }
   {}

   constexpr float getRad() const noexcept { return m_angleRad; }
   constexpr float getDeg() const noexcept { return m_angleRad * 180.f / std::numbers::pi_v<float>; }

   friend constexpr Angle operator+(const Angle& lhs, const Angle& rhs) noexcept
   {
      return Angle(lhs.getRad() + rhs.getRad());
   }

   friend constexpr Angle operator-(const Angle& lhs, const Angle& rhs) noexcept
   {
      return Angle(lhs.getRad() - rhs.getRad());
   }

   std::partial_ordering operator<=>(const Angle& other) const
   {
      if (!std::isfinite(m_angleRad) || !std::isfinite(other.m_angleRad))
      {
         return std::partial_ordering::unordered;
      }

      float diff{ m_angleRad - other.m_angleRad };
      if (isEqual(diff))
      {
         return std::partial_ordering::equivalent;
      }
      else if (m_angleRad < other.m_angleRad)
      {
         return std::partial_ordering::less;
      }
      else
      {
         return std::partial_ordering::greater;
      }
   }

   constexpr bool operator==(const Angle& other) const
   {
      float diff{ m_angleRad - other.m_angleRad };
      return isEqual(diff);
   }


private:
   constexpr bool isEqual(float diff) const
   {
      // TODO: better float comparison
      return (diff < std::numeric_limits<float>::epsilon()) && (-diff < std::numeric_limits<float>::epsilon());
   }

   float m_angleRad{ 0.f };
};

constexpr Angle operator ""_deg(long double deg) noexcept
{
   return Angle(static_cast<float>(deg * std::numbers::pi_v<float> / 180.f));
}

constexpr Angle operator ""_rad(long double rad) noexcept
{
   return Angle(static_cast<float>(rad));
}

} // namespace utils

namespace std
{

float sin(const utils::Angle& angle) noexcept { return std::sin(angle.getRad()); }
float cos(const utils::Angle& angle) noexcept { return std::cos(angle.getRad()); }
float tan(const utils::Angle& angle) noexcept { return std::tan(angle.getRad()); }

float sinh(const utils::Angle& angle) noexcept { return std::sinh(angle.getRad()); }
float cosh(const utils::Angle& angle) noexcept { return std::cosh(angle.getRad()); }
float tanh(const utils::Angle& angle) noexcept { return std::tanh(angle.getRad()); }

} // namespace std
