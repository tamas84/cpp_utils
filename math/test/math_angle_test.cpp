#include <gtest/gtest.h>

#include <angle.h>

#include <map>
#include <numbers>
#include <vector>

using namespace utils;

namespace test
{

class AngleTestFixture : public ::testing::Test
{
public:
   void SetUp() override
   {
   }

protected:
   const std::map<float, float> degRadAngles = {
      {0.f,  0.f},
      {15.f,  std::numbers::pi_v<float> / 12.f},
      {30.f,  std::numbers::pi_v<float> / 6.f},
      {45.f,  std::numbers::pi_v<float> / 4.f},
      {60.f,  std::numbers::pi_v<float> / 3.f},
      {90.f,  std::numbers::pi_v<float> / 2.f},
      {180.f, std::numbers::pi_v<float>},
      {360.f, 2.f * std::numbers::pi_v<float>}
   };

   const std::vector<Angle> angles{
      0.0_deg,
      15.0_deg,
      30.0_deg,
      45.0_deg,
      60.0_deg,
      90.0_deg,
      180.0_deg,
      360.0_deg
   };

};

TEST_F(AngleTestFixture, ctor)
{
   int idx{ 0 };
   for (const auto& [deg, rad] : degRadAngles)
   {
      EXPECT_FLOAT_EQ(rad, angles[idx].getRad());
      EXPECT_FLOAT_EQ(deg, angles[idx].getDeg());
      ++idx;
   }
}

TEST_F(AngleTestFixture, equal)
{
   int idx{ 0 };
   for (const auto& [deg, rad] : degRadAngles)
   {
      EXPECT_TRUE(Angle(rad) == angles[idx]);
      EXPECT_TRUE(angles[idx] == Angle(rad));
      ++idx;
   }
}

TEST_F(AngleTestFixture, not_equal)
{
   constexpr Angle minus10{ -(10.0_deg).getRad() };
   for (const auto& [deg, rad] : degRadAngles)
   {
      EXPECT_TRUE(Angle(rad) != minus10);
      EXPECT_TRUE(minus10 != Angle(rad));
   }
}

TEST_F(AngleTestFixture, less)
{
   constexpr Angle angle400{ 400.0_deg };
   for (const auto& [deg, rad] : degRadAngles)
   {
      EXPECT_TRUE(Angle(rad) < angle400);
   }
}

TEST_F(AngleTestFixture, greater)
{
   constexpr Angle minus10{ -(10.0_deg).getRad() };
   for (const auto& [deg, rad] : degRadAngles)
   {
      EXPECT_TRUE(Angle(rad) > minus10);
   }
}

TEST_F(AngleTestFixture, op_addition)
{
   constexpr Angle angle15{ 15.0_deg };
   constexpr Angle angle30{ 30.0_deg };

   constexpr Angle sumAngle{ angle15 + angle15 };
   EXPECT_TRUE(angle30 == sumAngle);
}

TEST_F(AngleTestFixture, op_subtraction)
{
   constexpr Angle angle15{ 15.0_deg };
   constexpr Angle angle30{ 30.0_deg };

   constexpr Angle sumAngle{ angle30 - angle15 };
   EXPECT_TRUE(angle15 == sumAngle);
}

} // namespace test
