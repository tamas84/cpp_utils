#include <gtest/gtest.h>
#include <sync_resource.h>

#include <vector>


namespace
{

using namespace utils;

struct TestType
{
   std::string foo() const { return "text"; }
};

template <typename T>
void add(Access<SyncResource<T>> access, T value)
{
   access.lock();
   (*access) += value;
   access.unlock();
}

class SyncResourceFixture : public ::testing::Test
{
public:
   SyncResourceFixture()
   {
   }

protected:
   SyncResource<int> sr_int = SyncResource<int>(5);
};

TEST_F(SyncResourceFixture, test_access_move)
{
   auto access1 = sr_int.getAccess();
   EXPECT_EQ(5, *access1);
   *access1 += 1;
   EXPECT_EQ(6, *access1);

   auto access2 = std::move(access1);
   EXPECT_EQ(6, *access1);
}

TEST_F(SyncResourceFixture, test_access_try)
{
   auto access1 = sr_int.getAccess();
   auto access2 = sr_int.tryGetAccess();
   std::cout << access2.isLocked() << std::endl;
   EXPECT_FALSE(access2.isLocked());
}

TEST_F(SyncResourceFixture, test_multiple_access)
{
   std::vector<std::thread> threads;
   threads.reserve(200);
   for (int i = 0; i < 100; ++i)
   {
      threads.emplace_back(add<int>, sr_int.getAccess(std::defer_lock_t{}),  1);
      threads.emplace_back(add<int>, sr_int.getAccess(std::defer_lock_t{}), -1);
   }

   for (auto& thread : threads)
   {
      thread.join();
   }

   EXPECT_EQ(5, sr_int.unsafe());
}


TEST_F(SyncResourceFixture, test_struct_as_T)
{
   SyncResource<TestType> sr = SyncResource<TestType>();
   auto access = sr.getAccess();
   EXPECT_EQ("text", (*access).foo());
   EXPECT_EQ("text", access->foo());
}

} // namespace
