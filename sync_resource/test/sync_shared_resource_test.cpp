#include <gtest/gtest.h>
#include <sync_shared_resource.h>

#include <shared_mutex>
#include <vector>


namespace
{

using namespace utils;

struct TestType
{
   std::string foo() const { return "text"; }
};

template <typename T>
void add(WriteAccess<SyncSharedResource<T, std::shared_mutex>> access, T value)
{
   access.lock();
   (*access) += value;
   access.unlock();
}

class SyncSharedResourceFixture : public ::testing::Test
{
public:
   SyncSharedResourceFixture()
   {
   }

protected:
   SyncSharedResource<int, std::shared_mutex> sshr_int = SyncSharedResource<int, std::shared_mutex>(5);
};

TEST_F(SyncSharedResourceFixture, test_access_lock)
{
   auto readAccess1 = sshr_int.getReadAccess();
   EXPECT_EQ(5, *readAccess1);
   auto readAccess2 = sshr_int.tryGetReadAccess();
   EXPECT_EQ(5, *readAccess2);

   auto writeAcces1 = sshr_int.tryGetWriteAccess();
   EXPECT_FALSE(writeAcces1.isLocked());

   readAccess2.unlock();
   readAccess1.unlock();
   auto writeAcces2 = sshr_int.tryGetWriteAccess();
   EXPECT_TRUE(writeAcces2.isLocked());
   *writeAcces2 = 6;
   EXPECT_EQ(6, *writeAcces2);

   auto writeAcces3 = sshr_int.tryGetWriteAccess();
   EXPECT_FALSE(writeAcces3.isLocked());
}

TEST_F(SyncSharedResourceFixture, test_readAccess_move)
{
   auto readAccess1 = sshr_int.getReadAccess();
   EXPECT_EQ(5, *readAccess1);

   auto readAccess2 = std::move(readAccess1);
   EXPECT_FALSE(readAccess1.isLocked());
   EXPECT_EQ(5, *readAccess2);
}

TEST_F(SyncSharedResourceFixture, test_multiple_writeAccess)
{
   std::vector<std::thread> threads;
   threads.reserve(200);
   for (int i = 0; i < 100; ++i)
   {
      threads.emplace_back(add<int>, sshr_int.getWriteAccess(std::defer_lock_t{}),  1);
      threads.emplace_back(add<int>, sshr_int.getWriteAccess(std::defer_lock_t{}), -1);
   }

   for (auto& thread : threads)
   {
      thread.join();
   }

   EXPECT_EQ(5, sshr_int.unsafe());
}

TEST_F(SyncSharedResourceFixture, test_struct_as_T)
{
   SyncSharedResource<TestType, std::shared_mutex> sr = SyncSharedResource<TestType, std::shared_mutex>();
   auto ra = sr.getReadAccess();
   EXPECT_EQ("text", (*ra).foo());
   EXPECT_EQ("text", ra->foo());
   ra.unlock();

   auto wa = sr.getWriteAccess();
   EXPECT_EQ("text", (*wa).foo());
   EXPECT_EQ("text", wa->foo());
}

} // namespace
