#include <chrono>
#include <mutex>


namespace utils
{

template <typename TValue, typename TSharedMutex>
   requires SharedMutexType<TSharedMutex>
class SyncSharedResource;

template <typename T>
concept SharedMutexType = requires(T t)
{
   { t.lock_shared() };
   { t.try_lock_shared() } -> std::convertible_to<bool>;
   { t.unlock_shared() };
};

template<typename T>
concept SyncSharedResourceType =
   std::same_as<T, SyncSharedResource<typename T::value_type, typename T::mutex_type>>;

template <typename TSyncSharedResource>
   requires SyncSharedResourceType<TSyncSharedResource>
class ReadAccess
{
public:
   ReadAccess(TSyncSharedResource& SyncSharedResource)
      : m_SyncSharedResource(SyncSharedResource)
      , m_owns(true)
   {
   }

   ReadAccess(TSyncSharedResource& SyncSharedResource, std::defer_lock_t)
      : m_SyncSharedResource(SyncSharedResource)
      , m_owns(false)
   {
   }

   ~ReadAccess()
   {
      unlock();
   }

   void lock()
   {
      if (!m_owns)
      {
         m_owns = true;
         const_cast<TSyncSharedResource&>(m_SyncSharedResource).m_mutex.lock_shared();
      }
   }

   void unlock()
   {
      if (m_owns)
      {
         m_owns = false;
         const_cast<TSyncSharedResource&>(m_SyncSharedResource).m_mutex.unlock_shared();
      }
   }

   TSyncSharedResource::value_type& unsafe() const
   {
      const_cast<TSyncSharedResource&>(m_SyncSharedResource).unsafe();
   }

   bool isLocked() const noexcept { return m_owns; }

   ReadAccess(const ReadAccess&) = delete;
   ReadAccess(ReadAccess&& o) noexcept
      : m_SyncSharedResource(o.m_SyncSharedResource)
      , m_owns(o.m_owns)
   {
      if (isLocked())
      {
         const_cast<TSyncSharedResource&>(o.m_SyncSharedResource).m_mutex.unlock_shared();
         o.m_owns = false;

         const_cast<TSyncSharedResource&>(m_SyncSharedResource).m_mutex.lock_shared();
      }
   }
   ReadAccess& operator=(const ReadAccess&) = delete;
   ReadAccess& operator=(ReadAccess&& o)
   {
      if (isLocked())
      {
         m_SyncSharedResource.m_mutex.unlock_shared();
      }

      m_SyncSharedResource = o.m_SyncSharedResource;
      m_owns = o.m_owns;
      if (isLocked())
      {
         o.m_SyncSharedResource.m_mutex.unlock_shared();
         o.m_owns = false;
      }
   }

   typename const TSyncSharedResource::value_type& operator* () const { return *m_SyncSharedResource; }
   typename const TSyncSharedResource::value_type* operator->() const { return m_SyncSharedResource.operator->(); }

private:
   const TSyncSharedResource& m_SyncSharedResource;
   bool m_owns;
};

template <typename TSyncSharedResource>
   requires SyncSharedResourceType<TSyncSharedResource>
class WriteAccess
{
public:
   WriteAccess(TSyncSharedResource& SyncSharedResource)
      : m_SyncSharedResource(SyncSharedResource)
      , m_owns(true)
   {
   }

   WriteAccess(TSyncSharedResource& SyncSharedResource, std::defer_lock_t)
      : m_SyncSharedResource(SyncSharedResource)
      , m_owns(false)
   {
   }

   ~WriteAccess()
   {
      unlock();
   }

   void lock()
   {
      if (!m_owns)
      {
         m_owns = true;
         m_SyncSharedResource.m_mutex.lock();
      }
   }

   void unlock()
   {
      if (m_owns)
      {
         m_owns = false;
         m_SyncSharedResource.m_mutex.unlock();
      }
   }

   bool isLocked() const noexcept { return m_owns; }

   WriteAccess(const WriteAccess& o) = delete;
   WriteAccess(WriteAccess&& o) noexcept
      : m_SyncSharedResource(o.m_SyncSharedResource)
      , m_owns(o.m_owns)
   {
      o.m_owns = false;
   }
   WriteAccess& operator=(const WriteAccess&) = delete;
   WriteAccess& operator=(WriteAccess&& o)
   {
      if (isLocked())
      {
         m_SyncSharedResource.m_mutex.unlock();
      }

      m_SyncSharedResource = o.m_SyncSharedResource;
      m_owns = o.m_owns;
      o.m_owns = false;
   }

   typename TSyncSharedResource::value_type& operator* () const { return *m_SyncSharedResource; }
   typename TSyncSharedResource::value_type* operator->() const { return m_SyncSharedResource.operator->(); }

private:
   TSyncSharedResource& m_SyncSharedResource;
   bool m_owns;
};

template <typename TValue, typename TSharedMutex>
   requires SharedMutexType<TSharedMutex>
class SyncSharedResource
{
   template <typename TSyncSharedResource>
      requires SyncSharedResourceType<TSyncSharedResource>
   friend class ReadAccess;

   template <typename TSyncSharedResource>
      requires SyncSharedResourceType<TSyncSharedResource>
   friend class WriteAccess;

public:
   using value_type = TValue;
   using mutex_type = TSharedMutex;

public:
   SyncSharedResource()
      : m_resource(TValue{})
   {}

   explicit SyncSharedResource(TValue v)
      : m_resource(std::move(v))
   {}

   auto getReadAccess() -> decltype(ReadAccess(*this))
   {
      m_mutex.lock_shared();
      return ReadAccess(*this);
   }

   auto getReadAccess(std::defer_lock_t t) -> decltype(ReadAccess(*this))
   {
      return getReadAccess(*this, t);
   }

   auto tryGetReadAccess() -> decltype(ReadAccess(*this))
   {
      if (m_mutex.try_lock_shared())
      {
         return ReadAccess(*this);
      }
      else
      {
         return ReadAccess(*this, std::defer_lock_t{});
      }
   }

   template<typename Rep, typename Period>
   auto tryGetReadAccessFor(const std::chrono::duration<Rep, Period>& timeoutDuration) -> decltype(ReadAccess(*this))
   {
      if (m_mutex.try_lock_shared_for(timeoutDuration))
      {
         return ReadAccess(*this);
      }
      else
      {
         return ReadAccess(*this, std::defer_lock_t{});
      }
   }

   template<typename Clock, typename Duration>
   auto tryGetReadAccessUntil(const std::chrono::time_point<Clock, Duration>& timeoutTime) -> decltype(ReadAccess(*this))
   {
      if (m_mutex.try_lock_shared_until(timeoutTime))
      {
         return ReadAccess(*this);
      }
      else
      {
         return ReadAccess(*this, std::defer_lock_t{});
      }
   }

   auto getWriteAccess() -> decltype(WriteAccess(*this))
   {
      m_mutex.lock();
      return WriteAccess(*this);
   }

   auto getWriteAccess(std::defer_lock_t t) -> decltype(WriteAccess(*this))
   {
      return WriteAccess(*this, t);
   }

   auto tryGetWriteAccess() -> decltype(WriteAccess(*this))
   {
      if (m_mutex.try_lock())
      {
         return WriteAccess(*this);
      }
      else
      {
         return WriteAccess(*this, std::defer_lock_t{});
      }
   }

   SyncSharedResource(const SyncSharedResource&) = delete;
   SyncSharedResource(SyncSharedResource&&) = delete;
   SyncSharedResource& operator=(const SyncSharedResource&) = delete;
   SyncSharedResource& operator=(SyncSharedResource&&) = delete;

   value_type& unsafe() { return m_resource; }

private:
   typename const TValue& operator* () const { return m_resource; }
   typename TValue& operator* () { return m_resource; }

   typename const TValue* operator->() const { return &m_resource; }
   typename TValue* operator->() { return &m_resource; }

private:
   TSharedMutex m_mutex;
   TValue m_resource;
};

} // namespace utils
