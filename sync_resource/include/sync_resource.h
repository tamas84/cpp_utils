#include <mutex>


namespace utils
{

template <typename T>
class SyncResource;

template<typename T>
concept SyncResourceType =
   std::same_as<T, SyncResource<typename T::value_type>>;

template <typename TSyncResource>
   requires SyncResourceType<TSyncResource>
class Access
{
public:
   Access(TSyncResource& SyncResource)
      : m_SyncResource(SyncResource)
      , m_owns(true)
   {
   }

   Access(TSyncResource& SyncResource, std::defer_lock_t)
      : m_SyncResource(SyncResource)
      , m_owns(false)
   {
   }

   ~Access()
   {
      unlock();
   }

   void lock()
   {
      if (!m_owns)
      {
         m_owns = true;
         m_SyncResource.m_mutex.lock();
      }
   }

   void unlock()
   {
      if (m_owns)
      {
         m_owns = false;
         m_SyncResource.m_mutex.unlock();
      }
   }

   bool isLocked() const noexcept { return m_owns; }

   Access(const Access&) = delete;
   Access(Access&& o) noexcept
      : m_SyncResource(o.m_SyncResource)
      , m_owns(o.m_owns)
   {
      o.m_owns = false;
   }
   Access& operator=(const Access&) = delete;
   Access& operator=(Access&& o)
   {
      m_SyncResource = o.m_SyncResource;
      m_owns = o.m_owns;
      o.m_owns = false;
   }

   typename TSyncResource::value_type& operator* () const { return *m_SyncResource; }
   typename TSyncResource::value_type* operator->() const { return m_SyncResource.operator->(); }

private:
   TSyncResource& m_SyncResource;
   bool m_owns;
};

template <typename TValue>
class SyncResource
{
   template <typename TSyncResource>
      requires SyncResourceType<TSyncResource>
   friend class Access;

public:
   using value_type = TValue;

public:
   SyncResource()
      : m_resource(TValue{})
   {}

   explicit SyncResource(TValue v)
      : m_resource(std::move(v))
   {}

   auto getAccess()
   {
      m_mutex.lock();
      return Access(*this);
   }

   auto getAccess(std::defer_lock_t t) -> decltype(Access(*this))
   {
      return Access(*this, t);
   }

   auto tryGetAccess() -> decltype(Access(*this))
   {
      if (m_mutex.try_lock())
      {
         return Access(*this);
      }
      else
      {
         return Access(*this, std::defer_lock_t{});
      }
   }

   SyncResource(const SyncResource&) = delete;
   SyncResource(SyncResource&&) = delete;
   SyncResource& operator=(const SyncResource&) = delete;
   SyncResource& operator=(SyncResource&&) = delete;

   value_type& unsafe() { return m_resource; }

private:
   typename TValue& operator* () { return m_resource; }
   typename TValue* operator->() { return &m_resource; }

private:
   std::mutex m_mutex;
   TValue m_resource;
};

} // namespace utils
