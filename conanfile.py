from conans import ConanFile, CMake, tools


class CppUtilsConan(ConanFile):
    settings = 'os', 'compiler', 'build_type', 'arch'
    generators = "cmake_find_package"

    def build(self):
        cmake = CMake(self) # it will find the packages by using our auto-generated FindXXX.cmake files
        cmake.configure()
        cmake.build()

    def requirements(self):
       self.requires("gtest/cci.20210126")
       self.requires("jsoncpp/1.9.5")
       self.requires("glm/0.9.9.8")
