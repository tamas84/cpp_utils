/*
Copyright (c) <2022> <Gergely, Tamas>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <functional> // std::hash

namespace utils
{

template <typename T>
class Delegate;

template <typename ReturnT, typename... Args>
class Delegate<ReturnT (Args...)>;

} // namespace utils

template <typename ReturnT, typename... Args>
struct std::hash<utils::Delegate<ReturnT (Args...)>>;

namespace utils
{

template <typename ReturnT, typename... Args>
class Delegate<ReturnT (Args...)>
{
   friend struct std::hash<Delegate<ReturnT (Args...)>>;
public:
   Delegate() = default;

   Delegate(const Delegate& other)
     : m_objectFreeFunctionPtr(other.m_objectFreeFunctionPtr)
     , m_memberFunctionPtr(other.m_memberFunctionPtr)
     , m_objectPtr(other.m_objectPtr)
   {
   }

   Delegate(Delegate&& other)
     : Delegate()
   {
      Swap(*this, other);
   }

   Delegate& operator=(Delegate other)
   {
      Swap(*this, other);
      return *this;
   }

   // for free and static memeber functions
   void Bind(ReturnT(*function)(Args...))
   {
      m_objectFreeFunctionPtr = function;
   }

   // for non-static member functions
   template <typename ClassT, ReturnT (ClassT::*member)(Args...) const>
   void Bind(ClassT* objectPtr)
   {
      Clear();
      m_objectPtr = objectPtr;
      m_memberFunctionPtr = &memberCaller<ClassT, member>;
   }

   template <typename ClassT, ReturnT (ClassT::*member)(Args...)>
   void Bind(ClassT* objectPtr)
   {
      Clear();
      m_objectPtr = objectPtr;
      m_memberFunctionPtr = &memberCaller<ClassT, member>;
   }

   // for functors and lambdas
   template <typename ClassT>
   void Bind(ClassT* objectPtr)
   {
      Clear();
      m_objectPtr = objectPtr;
      m_memberFunctionPtr = &memberCaller<ClassT, &ClassT::operator()>;
   }

   void Unbind() noexcept
   {
      m_objectFreeFunctionPtr = nullptr;
      m_objectPtr = nullptr;
      m_memberFunctionPtr = nullptr;
   }

   ReturnT operator()(Args... args) const
   {
      if (m_objectPtr)
      {
         return m_memberFunctionPtr(m_objectPtr, std::forward<Args>(args)...);
      }
      else
      {
         return m_objectFreeFunctionPtr(std::forward<Args>(args)...);
      }
   }

   explicit operator bool() const noexcept
   {
      return (m_objectPtr || m_objectFreeFunctionPtr);
   }

   bool IsEmpty() const noexcept
   {
      return !static_cast<bool>(*this);
   }

   bool operator<(const Delegate<ReturnT (Args...)>& o) const
   {
      if (!m_objectPtr && o.m_objectPtr)
      {
         return true;
      }
      if (m_objectPtr && !o.m_objectPtr)
      {
         return false;
      }
      if (!m_objectPtr && !o.m_objectPtr)
      {
         return (m_objectFreeFunctionPtr < o.m_objectFreeFunctionPtr);
      }

      if (m_objectPtr < o.m_objectPtr)
      {
         return true;
      }
      else if (o.m_objectPtr < m_objectPtr)
      {
         return false;
      }
      else
      {
         return m_memberFunctionPtr < o.m_memberFunctionPtr;
      }
   }

   bool operator==(const Delegate<ReturnT (Args...)>& o) const
   {
      if ((m_objectPtr && !o.m_objectPtr)
          || (!m_objectPtr && o.m_objectPtr))
      {
         return false;
      }

      if (!m_objectPtr && !o.m_objectPtr)
      {
         return m_objectFreeFunctionPtr == o.m_objectFreeFunctionPtr;
      }

      if (m_objectPtr != o.m_objectPtr)
      {
         return false;
      }
      else
      {
         return m_memberFunctionPtr == o.m_memberFunctionPtr;
      }
   }

private:
   // for free and static memeber functions
   //ReturnT (*m_obectFreeFunctionPtr)(Args...);
   using ObjectFreeFunction = ReturnT (Args...); // typedef ReturnT ObjectFreeFunction(Args...);
   ObjectFreeFunction* m_objectFreeFunctionPtr = nullptr;

   // for member functions
   //ReturnT (*m_memberFunctionPtr)(void* objectPtr, Args&&...);
   using MemberFunction = ReturnT (void* objectPtr, Args...);
   MemberFunction* m_memberFunctionPtr;
   void* m_objectPtr = nullptr;

   template <typename ClassT, ReturnT (ClassT::*MemberT)(Args...) const>
   static ReturnT memberCaller(void* objectPtr, Args ...args)
   {
      const auto* ptr = static_cast<const ClassT*>(objectPtr);
      return (ptr->*MemberT)(std::forward<Args>(args)...);
   }

   template <typename ClassT, ReturnT (ClassT::*MemberT)(Args...)>
   static ReturnT memberCaller(void* objectPtr, Args ...args)
   {
      auto* ptr = static_cast<ClassT*>(objectPtr);
      return (ptr->*MemberT)(std::forward<Args>(args)...);
   }

   void Clear()
   {
      m_objectPtr = nullptr;
      m_objectFreeFunctionPtr = nullptr;
      m_memberFunctionPtr = nullptr;
   }

   friend void Swap(Delegate<ReturnT (Args...)>& left, Delegate<ReturnT (Args...)>& right) noexcept
   {
      std::swap(left.m_objectPtr, right.m_objectPtr);
      std::swap(left.m_objectFreeFunctionPtr, right.m_objectFreeFunctionPtr);
      std::swap(left.m_memberFunctionPtr, right.m_memberFunctionPtr);
   }
};

} // namespace utils

namespace std
{

template <typename ReturnT, typename... Args>
struct hash<utils::Delegate<ReturnT (Args...)>>
{
   std::size_t operator()(const utils::Delegate<ReturnT (Args...)>& key) const
   {
      if (key.m_objectPtr)
      {
         return reinterpret_cast<std::size_t>(key.m_memberFunctionPtr);
      }
      else
      {
         return reinterpret_cast<std::size_t>(key.m_objectFreeFunctionPtr);
      }
   }
};

} // namespace std
