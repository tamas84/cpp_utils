#include "delegate_test_types.h"

#include <delegate.h>
#include <gtest/gtest.h>

namespace
{

using namespace utils;
using namespace test;

TEST(DelegateTest, copyCtor)
{
   Delegate<int(int)> delegate1;
   delegate1.Bind(&globalFunction);
   EXPECT_TRUE(delegate1);

   auto delegate2 = delegate1;
   EXPECT_TRUE(delegate1);
   EXPECT_TRUE(delegate2);
}

TEST(DelegateTest, copyAssign)
{
   Delegate<int(int)> delegate1;
   delegate1.Bind(&globalFunction);
   EXPECT_TRUE(delegate1);

   Delegate<int(int)> delegate2;
   EXPECT_FALSE(delegate2);
   delegate2 = delegate1;
   EXPECT_TRUE(delegate1);
   EXPECT_TRUE(delegate2);
}

TEST(DelegateTest, move)
{
   Delegate<int(int)> delegate1;
   delegate1.Bind(&globalFunction);
   EXPECT_TRUE(delegate1);

   auto delegate2 = std::move(delegate1);
   EXPECT_FALSE(delegate1);
   EXPECT_TRUE(delegate2);
}

TEST(DelegateTest, moveAssign)
{
   Delegate<int(int)> delegate1;
   delegate1.Bind(&globalFunction);
   EXPECT_TRUE(delegate1);

   Delegate<int(int)> delegate2;
   EXPECT_FALSE(delegate2);
   delegate2 = std::move(delegate1);
   EXPECT_FALSE(delegate1);
   EXPECT_TRUE(delegate2);
}

/////////////////////////////////////////
// Test call to
// ReturnT operator()(Args... args) const
// with lvalue and rvalue references
// Note Args is the template type parameter of the class not the function
// so here Args&& would mean an rvalue reference

TEST(DelegateTest, copyParameter_freeFunc)
{
   Delegate<void(Type)> delegate;
   delegate.Bind(&copyMove);
   EXPECT_TRUE(delegate);

   Type obj;
   delegate(obj);
}

TEST(DelegateTest, moveParameter_freeFunc)
{
   Delegate<void(Type&&)> delegate;
   delegate.Bind(&copyMove);
   EXPECT_TRUE(delegate);

   Type obj;
   delegate(std::move(obj));
}

TEST(DelegateTest, copyParameter_memberFunc)
{
   Delegate<void(Type)> delegate;

   CopyMoveType typeObj;
   delegate.Bind<CopyMoveType, &CopyMoveType::copyMove>(&typeObj);
   EXPECT_TRUE(delegate);

   Type obj;
   delegate(obj);
}

TEST(DelegateTest, move_with_memberFunc)
{
   Delegate<void(Type&&)> delegate;

   CopyMoveType typeObj;
   delegate.Bind<CopyMoveType, &CopyMoveType::copyMove>(&typeObj);
   EXPECT_TRUE(delegate);

   Type obj;
   delegate(std::move(obj));
}

} // namespace
