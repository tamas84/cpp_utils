#include "delegate_test_types.h"

#include <delegate.h>
#include <gtest/gtest.h>

namespace
{

using namespace utils;
using namespace test;

TEST(DelegateTest, empty)
{
   Delegate<int(int)> delegate;
   EXPECT_FALSE(delegate);

   delegate.Unbind();
   EXPECT_FALSE(delegate);
   EXPECT_TRUE(delegate.IsEmpty());
}

TEST(DelegateTest, empty_freeFunction)
{
   Delegate<int(int)> delegate;

   delegate.Bind(&globalFunction);
   EXPECT_TRUE(delegate);
   EXPECT_FALSE(delegate.IsEmpty());

   delegate.Unbind();
   EXPECT_FALSE(delegate);
   EXPECT_TRUE(delegate.IsEmpty());
}

TEST(DelegateTest, empty_memberFunction)
{
   Delegate<int(int)> delegate;

   Type obj;
   delegate.Bind<Type, &Type::member>(&obj);
   EXPECT_TRUE(delegate);
   EXPECT_FALSE(delegate.IsEmpty());

   delegate.Unbind();
   EXPECT_FALSE(delegate);
   EXPECT_TRUE(delegate.IsEmpty());
}

TEST(DelegateTest, freeFunction)
{
   Delegate<int(int)> delegate;
   delegate.Bind(&globalFunction);
   EXPECT_EQ(5, delegate(5));
}

TEST(DelegateTest, constOverload)
{
   ConstOverloadType obj;
   Delegate<int(int)> delegate;
   delegate.Bind<ConstOverloadType, static_cast<int (ConstOverloadType::*)(int)>(&ConstOverloadType::member)>(&obj);
   EXPECT_EQ(5, delegate(5));
   delegate.Bind<ConstOverloadType, static_cast<int (ConstOverloadType::*)(int) const>(&ConstOverloadType::member)>(&obj);
   EXPECT_EQ(10, delegate(5));
}

TEST(DelegateTest, nonConstMemberFunction)
{
   Type obj;
   Delegate<int(int)> delegate;
   delegate.Bind<Type, &Type::member>(&obj);
   EXPECT_EQ(5, delegate(5));
}

TEST(DelegateTest, constMemberFunction)
{
   Type obj;
   Delegate<int(int)> delegate;
   delegate.Bind<Type, &Type::constMember>(&obj);
   EXPECT_EQ(5, delegate(5));
}

TEST(DelegateTest, virtualNonConstMemberFunction)
{
   Type obj;
   Delegate<int(int)> delegate;
   delegate.Bind<Type, &Type::virtualMember>(&obj);
   EXPECT_EQ(5, delegate(5));
}

TEST(DelegateTest, virtualConstMemberFunction)
{
   Type obj;
   Delegate<int(int)> delegate;
   delegate.Bind<Type, &Type::constVirtualMember>(&obj);
   EXPECT_EQ(5, delegate(5));
}

TEST(DelegateTest, derivedVirtualNonConstMemberFunction)
{
   DerivedType obj;
   Delegate<int(int)> delegate;
   delegate.Bind<DerivedType, &DerivedType::virtualMember>(&obj);
   EXPECT_EQ(10, delegate(5));

   delegate.Bind<Type, &Type::virtualMember>(&obj);
   EXPECT_EQ(10, delegate(5));
}

TEST(DelegateTest, derivedVirtualConstMemberFunction)
{
   DerivedType obj;
   Delegate<int(int)> delegate;
   delegate.Bind<DerivedType, &DerivedType::constVirtualMember>(&obj);
   EXPECT_EQ(10, delegate(5));

   delegate.Bind<Type, &Type::virtualMember>(&obj);
   EXPECT_EQ(10, delegate(5));
}

TEST(DelegateTest, noexceptMemberFunction)
{
   Type obj;
   Delegate<int(int)> delegate;
   delegate.Bind<Type, &Type::noexceptMember>(&obj);
   EXPECT_EQ(5, delegate(5));
}

TEST(DelegateTest, constNoexceptMemberFunction)
{
   Type obj;
   Delegate<int(int)> delegate;
   delegate.Bind<Type, &Type::constNoexceptMember>(&obj);
   EXPECT_EQ(5, delegate(5));
}

TEST(DelegateTest, staticMemberFunction)
{
   Delegate<int(int)> delegate;
   delegate.Bind(Type::staticMember);
   EXPECT_EQ(5, delegate(5));
}

TEST(DelegateTest, functor)
{
   Functor functor;
   Delegate<int(int)> delegate;
   delegate.Bind<Functor>(&functor);
   EXPECT_EQ(5, delegate(5));
}

TEST(DelegateTest, constFunctor)
{
   ConstFunctor functor;
   Delegate<int(int)> delegate;
   delegate.Bind<ConstFunctor>(&functor);
   EXPECT_EQ(5, delegate(5));
}

TEST(DelegateTest, lambda)
{
   auto lambda = [](int i) { return i; };
   Delegate<int(int)> delegate;
   delegate.Bind(&lambda);
   EXPECT_EQ(5, delegate(5));
}

TEST(DelegateTest, inheritanceBaseNonConst)
{
   Type obj;
   Delegate<int(Type*)> delegate;
   delegate.Bind(&typeF);
   EXPECT_EQ(5, delegate(&obj));
}

TEST(DelegateTest, inheritanceBaseConst)
{
   Type obj;
   Delegate<int(const Type*)> delegate;
   delegate.Bind(&typeConstF);
   EXPECT_EQ(5, delegate(&obj));
}

TEST(DelegateTest, inheritanceDerivedNonConst)
{
   DerivedType obj;
   Delegate<int(Type*)> delegate;
   delegate.Bind(&typeF);
   EXPECT_EQ(10, delegate(&obj));
}

TEST(DelegateTest, inheritanceDerivedConst)
{
   DerivedType obj;
   Delegate<int(const Type*)> delegate;
   delegate.Bind(&typeConstF);
   EXPECT_EQ(10, delegate(&obj));
}

} // namespace
