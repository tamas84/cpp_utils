#include "delegate_test_types.h"

#include <delegate.h>

#include <unordered_set>
#include <set>
#include <vector>
#include <gtest/gtest.h>

namespace
{

using namespace utils;
using namespace test;

TEST(DelegateTest, vector)
{
   Delegate<int(int)> staticMemberDelegate;
   staticMemberDelegate.Bind(Type::staticMember);

   Functor functor;
   Delegate<int(int)> functorDelegate;
   functorDelegate.Bind<Functor>(&functor);

   std::vector<Delegate<int(int)>> vector;
   vector.push_back(staticMemberDelegate);
   vector.push_back(functorDelegate);
   int sum{};
   for(auto& delegate : vector)
   {
      sum += delegate(5);
   }
   EXPECT_EQ(10, sum);
}

TEST(DelegateTest, assocContainer)
{
   Delegate<int(int)> staticMemberDelegate;
   staticMemberDelegate.Bind(Type::staticMember);

   Functor functor;
   Delegate<int(int)> functorDelegate;
   functorDelegate.Bind<Functor>(&functor);

   std::set<Delegate<int(int)>> set;
   set.insert(staticMemberDelegate);
   set.insert(functorDelegate);
   int sum{};
   for(auto& delegate : set)
   {
      sum += delegate(5);
   }
   EXPECT_EQ(10, sum);
}

TEST(DelegateTest, unorderedAssocContainer)
{
   Delegate<int(int)> staticMemberDelegate;
   staticMemberDelegate.Bind(Type::staticMember);

   Functor functor;
   Delegate<int(int)> functorDelegate;
   functorDelegate.Bind<Functor>(&functor);

   std::unordered_set<Delegate<int(int)>> set;
   set.insert(staticMemberDelegate);
   set.insert(functorDelegate);
   int sum{};
   for(auto& delegate : set)
   {
      sum += delegate(5);
   }
   EXPECT_EQ(10, sum);
}

TEST(DelegateTest, addAndDeleteFromAssocContainer)
{
   Delegate<int(int)> delegate1;
   delegate1.Bind(&globalFunction);

   Type obj;
   Delegate<int(int)> delegate2;
   delegate2.Bind<Type, &Type::member>(&obj);

   std::set<Delegate<int(int)>> set;
   set.insert(delegate1);
   set.insert(delegate1);
   EXPECT_EQ(1, set.size());

   set.insert(delegate2);
   EXPECT_EQ(2, set.size());

   set.erase(delegate1);
   EXPECT_EQ(1, set.size());
   set.erase(delegate2);
   EXPECT_EQ(0, set.size());
}

TEST(DelegateTest, addAndDeleteFromUnorderedAssocContainer)
{
   Delegate<int(int)> delegate1;
   delegate1.Bind(&globalFunction);

   Type obj;
   Delegate<int(int)> delegate2;
   delegate2.Bind<Type, &Type::member>(&obj);

   std::unordered_set<Delegate<int(int)>> set;
   set.insert(delegate1);
   set.insert(delegate1);
   EXPECT_EQ(1, set.size());

   set.insert(delegate2);
   EXPECT_EQ(2, set.size());

   set.erase(delegate1);
   EXPECT_EQ(1, set.size());
   set.erase(delegate2);
   EXPECT_EQ(0, set.size());
}

//TEST(DelegateTest, hash)
//{
//   Delegate<int(int)> delegate;
//   delegate.Bind(f);
//   std::hash<Delegate<int(int)>> hash;
//   std::cout << hash(delegate) << std::endl;

//   delegate.Bind(g);
//   std::cout << hash(delegate) << std::endl;

//   Type obj;
//   delegate.Bind<Type, &Type::f>(&obj);
//   std::cout << hash(delegate) << std::endl;
//   delegate.Bind<Type, &Type::constF>(&obj);
//   std::cout << hash(delegate) << std::endl;
//   delegate.Bind<Type, &Type::virtualF>(&obj);
//   std::cout << hash(delegate) << std::endl;
//   delegate.Bind<Type, &Type::virtualConstF>(&obj);
//   std::cout << hash(delegate) << std::endl;

//   DerivedType objDerived;
//   delegate.Bind<DerivedType, &DerivedType::virtualF>(&objDerived);
//   std::cout << hash(delegate) << std::endl;
//   delegate.Bind<DerivedType, &DerivedType::virtualConstF>(&objDerived);
//   std::cout << hash(delegate) << std::endl;
//}

} // namespace
