#pragma once

namespace test
{

/// Callables with
///  - int(int) signature
///  - non-const, const, noexcept
///
inline int globalFunction(int i)
{
   return i;
}

struct Type
{
   int member(int i)
   {
      return i;
   }

   int constMember(int i) const
   {
      return i;
   }

   virtual int virtualMember(int i)
   {
      return i;
   }

   virtual int constVirtualMember(int i) const
   {
      return i;
   }

   int noexceptMember(int i) noexcept
   {
      return i;
   }

   int constNoexceptMember(int i) const noexcept
   {
      return i;
   }

   static int staticMember(int i)
   {
      return i;
   }

   virtual ~Type() = default;
};

struct DerivedType : public Type
{
   virtual int virtualMember(int i) override
   {
      return 2 * i;
   }

   virtual int constVirtualMember(int i) const override
   {
      return 2 * i;
   }
};

struct ConstOverloadType
{
   int member(int i)
   {
      return i;
   }

   int member(int i) const
   {
      return 2 * i;
   }
};

struct Functor
{
   int operator()(int i)
   {
      return i;
   }
};

struct ConstFunctor
{
   int operator()(int i) const
   {
      return i;
   }
};

/// Functions that have a (const) Type* parameter
inline int typeF(Type* p)
{
   return p->virtualMember(5);
}

inline int typeConstF(const Type* p)
{
   return p->constVirtualMember(5);
}

inline void copyMove(Type t)
{}

inline void copyMove(Type&& t)
{}

struct CopyMoveType
{
   void copyMove(Type t)
   {}
   void copyMove(Type&& t)
   {}
};

} // namespace test
