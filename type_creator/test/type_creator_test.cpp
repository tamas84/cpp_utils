#include <gtest/gtest.h>

#include <type_creator.h>

#include <atomic>


namespace test
{

struct IType1
{
   virtual ~IType1() {}
   virtual int getIntValue() const = 0;
};

struct IType2
{
   virtual ~IType2() {}
   virtual double getDoubleValue() const = 0;
};

struct IType3
{
   virtual ~IType3() {}
   virtual std::string getStringValue() const = 0;
};

struct NotRegistered
{
   virtual ~NotRegistered() {}
};

struct Type1_ImplA : public IType1
{
   int getIntValue() const override
   {
      return 10;
   }
};

struct Type1_ImplB : public IType1
{
   int getIntValue() const override
   {
      return 11;
   }
};

struct Type2_ImplA : public IType2
{
   double getDoubleValue() const override
   {
      return 20.5;
   }
};

struct Type2_ImplB : public IType2
{
   double getDoubleValue() const override
   {
      return 21.5;
   }
};

struct Type3_ImplA : public IType3
{
   Type3_ImplA(){}
   Type3_ImplA(int){}
   std::string getStringValue() const override
   {
      return "Type3_ImplA";
   }
};


class TypeCreatorTestFixture : public ::testing::Test
{
protected:
   virtual void SetUp()
   {
      std::function<IType1*()> f = []()-> IType1* { return new Type1_ImplA(); };
      std::function<IType2*()> g = []()-> IType2* { return new Type2_ImplA(); };
      creator.registerType<IType1>(f);
      creator.registerType<IType2>(g);
   }

   virtual void TearDown()
   {
      creator.tearDown();
   }

   TypeCreator& creator = TypeCreator::getInstance();
};



TEST_F(TypeCreatorTestFixture, createAsRaw)
{
   IType1* obj1 = creator.createAsRawPtr<IType1>();
   EXPECT_EQ(10, obj1->getIntValue());
   delete obj1;

   IType2* obj2 = creator.createAsRawPtr<IType2>();
   EXPECT_DOUBLE_EQ(20.5, obj2->getDoubleValue());
   delete obj2;
}

TEST_F(TypeCreatorTestFixture, createAsUniquePtr)
{
   std::unique_ptr<IType1> ptr1 = creator.createAsUniquePtr<IType1>();
   EXPECT_EQ(10, ptr1->getIntValue());

   std::unique_ptr<IType2> ptr2 = creator.createAsUniquePtr<IType2>();
   EXPECT_DOUBLE_EQ(20.5, ptr2->getDoubleValue());
}

TEST_F(TypeCreatorTestFixture, cannotOverwriteSameType)
{
   // adding same base type does not overwrite
   std::function<IType1*()>  f = []()-> IType1* { return new Type1_ImplB(); };
   creator.registerType<IType1>(f);
   auto obj = creator.createAsUniquePtr<IType1>();
   EXPECT_EQ(10, obj->getIntValue()); // Type1_ImplB's getIntValue would return 11
}

TEST_F(TypeCreatorTestFixture, tearDown)
{
   creator.tearDown();

   IType1* obj = creator.createAsRawPtr<IType1>();
   EXPECT_EQ(nullptr, obj);
}

TEST_F(TypeCreatorTestFixture, tearDown_unique_ptr)
{
   creator.tearDown();

   std::unique_ptr<IType1> ptr = creator.createAsUniquePtr<IType1>();
   EXPECT_EQ(nullptr, ptr);
}

TEST_F(TypeCreatorTestFixture, createNotRegisteredType)
{
   std::unique_ptr<NotRegistered> ptr = creator.createAsUniquePtr<NotRegistered>();
   EXPECT_EQ(nullptr, ptr);
}

TEST_F(TypeCreatorTestFixture, test_typeExistsArgumentDoesNotMatch_zeroArgumentExpected_moreGiven)
{
   std::function<IType3*()> f = []()-> IType3* { return new Type3_ImplA(); };
   creator.registerType<IType3>(f);
   std::unique_ptr<IType3> ptrNotUsed = creator.createAsUniquePtr<IType3>("DummyString", 5, 6.6, 7.7f);
   EXPECT_EQ(nullptr, ptrNotUsed);
}

TEST_F(TypeCreatorTestFixture, test_typeExistsArgumentDoesNotMatch_intArgumentExpected_nonGiven)
{
   std::function<IType3*(int)> f = [](int i) -> IType3* { return new Type3_ImplA(i); };
   creator.registerType<IType3>(f);
   std::unique_ptr<IType3> ptrNotUsed = creator.createAsUniquePtr<IType3>();
   EXPECT_EQ(nullptr, ptrNotUsed);
}

} // namespace test

