#pragma once

#include <any>
#include <functional>
#include <iostream>

namespace
{
template <typename T>
void printParam(const T& t)
{
   std::cout << "\tParam type: " << typeid(T).name() << " value: " << t << std::endl;
}
}

class Item
{
public:
   Item(){}

   template<typename F>
   void setCreateFunc(F createFunc)
   {
      m_createFunc = createFunc;
   }

   template<typename T, typename... Args>
   T* getAsRawPtr(Args... args)
   {
      if (!m_createFunc.has_value())
      {
         std::cout << "!m_createFunc.has_value()" << std::endl;
         return nullptr;
      }

      try
      {
         auto cb = std::any_cast<std::function<T*(Args...)>>(m_createFunc);
         return cb(std::forward<Args>(args)...);
      }
      catch(const std::bad_any_cast&)
      {
         std::cout << m_createFunc.type().name() << std::endl;
         constexpr size_t size = sizeof... (args);
         //if (size)
         //{
            ((void) printParam(std::forward<Args>(args)), ...);
         //}
         return nullptr;
      }
   }

private:
   std::any m_createFunc;
};
