#pragma once

#include "item.h"

#include <functional>
#include <memory>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>


class TypeCreator
{
public:
   static TypeCreator& getInstance()
   {
      static TypeCreator instance;
      return instance;
   }

   static void tearDown()
   {
      auto& creator = getInstance();
      creator.m_factory.clear();
   }

   template<typename T, typename F>
   void registerType(F createFunc)
   {
      Item item;
      item.setCreateFunc<F>(createFunc);
      m_factory.emplace(std::type_index(typeid(T)), item);
   }

   template<typename T, typename... Args>
   T* createAsRawPtr(Args... args)
   {
      auto it = m_factory.find(std::type_index(typeid(T)));
      if (m_factory.end() == it)
      {
         return nullptr;
      }
      else
      {
         return (it->second).getAsRawPtr<T, Args...>(std::forward<Args>(args)...);
      }
   }

   template<typename T, typename... Args>
   std::unique_ptr<T> createAsUniquePtr(Args... args)
   {
      T* raw = createAsRawPtr<T>(std::forward<Args>(args)...);
      return std::unique_ptr<T>(raw);
   }

private:
   std::unordered_map<std::type_index, Item> m_factory;

private:
   TypeCreator(){}

public:
   TypeCreator(const TypeCreator&) = delete;
   TypeCreator(TypeCreator&&) = delete;
   TypeCreator& operator=(const TypeCreator&) = delete;
   TypeCreator& operator=(TypeCreator&&) = delete;
};
