#pragma once

#include <random>

namespace utils
{

class RandomBase
{
   friend class Random;
public:
   float operator()(float factor = m_defaultFactor)
   {
      return (static_cast<float>(m_distribution(m_generator)) / m_distribution.max()) * factor;
   }

protected:
   static constexpr float m_defaultFactor = 1.0f;
   std::mt19937 m_generator;
   std::uniform_int_distribution<std::mt19937::result_type> m_distribution;

protected:
   RandomBase(uint32_t value)
      : m_generator(value)
      , m_distribution()
   {}
};

class DeterministicRandom final : public RandomBase
{
public:
   DeterministicRandom(uint32_t initValue)
      : RandomBase(initValue)
   {
   }

   float next(float factor = m_defaultFactor)
   {
      return operator()(factor);
   }
};

class Random final
{
public:
   static float next(float factor = RandomBase::m_defaultFactor)
   {
      static thread_local RandomBase random = RandomBase(std::random_device{}());
      return random(factor);
   }

public:
   Random() = delete;
};

} // namespace utils
