#include <gtest/gtest.h>

#include <random_types.h>

using namespace utils;

namespace test
{

TEST(DeterministicRandomTest, deterministicRandomTest_funcCallOperator)
{
   DeterministicRandom dr1{20};
   float value1 = dr1();
   EXPECT_TRUE(value1 >= 0.0f && value1 <= 1.0f);

   DeterministicRandom dr2{20};
   float value2 = dr2();
   EXPECT_FLOAT_EQ(value1, value2);

   value1 = dr1(200);
   EXPECT_TRUE(value1 >= 0.0f && value1 <= 200.0f);
   value2 = dr2(200);
   EXPECT_FLOAT_EQ(value1, value2);
}

TEST(DeterministicRandomTest, deterministicRandomTest_funcNext)
{
   DeterministicRandom dr1{20};
   float value1 = dr1.next();
   EXPECT_TRUE(value1 >= 0.0f && value1 <= 1.0f);

   DeterministicRandom dr2{20};
   float value2 = dr2.next();
   EXPECT_FLOAT_EQ(value1, value2);

   value1 = dr1.next(200);
   EXPECT_TRUE(value1 >= 0.0f && value1 <= 200.0f);
   value2 = dr2.next(200);
   EXPECT_FLOAT_EQ(value1, value2);
}

TEST(NonDeterministicRandomTest, nondeterministicRandomTest)
{
   float value = Random::next();
   EXPECT_TRUE(value >= 0.0f && value <= 1.0f);

   value = Random::next(200);
   EXPECT_TRUE(value >= 0.0f && value <= 200.0f);
}

} // namespace test
