#include <singleton_factory.h>

template<typename T>
std::unique_ptr<T> SingletonFactory<T>::m_factory = nullptr;
