#include <gtest/gtest.h>

#include <singleton_factory.h>


struct IMySingleton
{
   ~IMySingleton() = default;
   virtual int getValue() = 0;

   // must be public OR (private AND friend to impl and mock)
   //  (the ctor of non abstract Singletons can be private)
   IMySingleton() = default;
};

struct MySingletonImpl : public IMySingleton
{
   static IMySingleton& getInstance()
   {
      static MySingletonImpl instance;
      return instance;
   }

   int getValue() override
   {
      return 1;
   }

private:
   MySingletonImpl() = default;
};

struct MySingletonAnotherImpl : public IMySingleton
{
   static IMySingleton& getInstance()
   {
      static MySingletonAnotherImpl instance;
      return instance;
   }

   int getValue() override
   {
      return 2;
   }

private:
   MySingletonAnotherImpl() = default;
};

struct IMySingletonFactory
{
   virtual ~IMySingletonFactory() = default;
   virtual IMySingleton& getInstance() = 0;
   typedef IMySingleton obj_type;
};

struct MySingletonImplFactory : public IMySingletonFactory
{
   MySingletonImplFactory() { std::cout << "MySingletonImplFactory" << std::endl; }
   ~MySingletonImplFactory() { std::cout << "~MySingletonImplFactory" << std::endl; }

   IMySingleton& getInstance() override
   {
      return MySingletonImpl::getInstance();
   }
};

struct MySingletonAnotherImplFactory : public IMySingletonFactory
{
   MySingletonAnotherImplFactory() { std::cout << "MySingletonAnotherImplFactory" << std::endl; }
   ~MySingletonAnotherImplFactory() { std::cout << "~MySingletonAnotherImplFactory" << std::endl; }

   IMySingleton& getInstance() override
   {
      return MySingletonAnotherImpl::getInstance();
   }
};

struct ClassToBeTested
{
   int doSomething()
   {
      IMySingleton& s = SingletonFactory<IMySingletonFactory>::getInstance();
      return s.getValue();
   }
};



TEST(SingletonFactoryTest, basicTest)
{
   SingletonFactory<IMySingletonFactory> f;
   ClassToBeTested c;

   // impl
   f.set( std::make_unique<MySingletonImplFactory>() );
   EXPECT_EQ(1, c.doSomething());

   // another impl
   f.set( std::make_unique<MySingletonAnotherImplFactory>() );
   EXPECT_EQ(2, c.doSomething());
}

template<>
std::unique_ptr<IMySingletonFactory> SingletonFactory<IMySingletonFactory>::m_factory = nullptr;
