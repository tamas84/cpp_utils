cmake_minimum_required(VERSION 3.5)

project(singleton_factory LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_library(${PROJECT_NAME} ${CMAKE_CURRENT_SOURCE_DIR}/src/singleton_factory.cpp)

target_include_directories(${PROJECT_NAME} PUBLIC  "$<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include>"
                                                     "$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>")

if(CPP_UTILS_ENABLE_TEST)
  add_subdirectory(test)
endif()
