#pragma once

#include <iostream>
#include <memory>
#include <typeinfo>

template<typename T>
struct SingletonFactory
{
    static std::unique_ptr<T> m_factory;

    static T& getFactory()
    {
        if (!m_factory)
        {
           std::cout << "Factory is not set for type: " << typeid(T).name() << std::endl;
        }
        return *m_factory;
    }

    static typename T::obj_type& getInstance()
    {
        if (!m_factory)
        {
           std::cout << "Factory is not set for type: " << typeid(T).name() << std::endl;
        }
        return m_factory->getInstance();
    }

    void set(std::unique_ptr<T> factory)
    {
        m_factory = std::move(factory);
    }
};
