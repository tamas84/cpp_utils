#pragma once

namespace utils
{

#include <functional>  // std::reference_wrapper
#include <type_traits> // std::remove_const, std::remove_reference

template <typename T>
class View
{
   using NonRefT      = typename std::remove_reference<T>::type;
   using ConstNonRefT = const typename std::remove_const<NonRefT>::type;

public:
   View(NonRefT& obj)
      : m_view{ obj }
   {}

   NonRefT& operator()() const { return m_view.get(); }
   View<ConstNonRefT> makeConstView() { return View<ConstNonRefT>(m_view.get()); }

private:
   std::reference_wrapper<NonRefT> m_view;
};

} // namespace utils
