#include <gtest/gtest.h>

#include <view.h>

namespace
{

struct MyType
{
   int m_int1{ 3 };
   int m_int2{ 4 };
};

} // namespace

using namespace utils;

namespace test
{

class ViewFixture : public ::testing::Test
{
protected:
   using ConstView   = View<const MyType>;
   using MutableView = View<MyType>;

   using OtherConstView = View<const MyType&>;
   using OtherMutableView = View<MyType&>;

public:
   void SetUp() override
   {
   }

protected:
   MyType obj;

   ConstView constView{ obj };
   MutableView mutableView{ obj };

   OtherConstView otherConstView{ obj };
   OtherMutableView otherMutableView{ obj };
};

TEST_F(ViewFixture, immutable)
{
   EXPECT_EQ(3, obj.m_int1);
   EXPECT_EQ(4, obj.m_int2);

   EXPECT_EQ(3, constView().m_int1);
   EXPECT_EQ(4, constView().m_int2);

   EXPECT_EQ(3, otherConstView().m_int1);
   EXPECT_EQ(4, otherConstView().m_int2);

   //constView().m_int1 = 42; // does not compile (good!)
}

TEST_F(ViewFixture, read_update_mutable)
{
   EXPECT_EQ(3, obj.m_int1);
   EXPECT_EQ(4, obj.m_int2);

   EXPECT_EQ(3, mutableView().m_int1);
   EXPECT_EQ(4, mutableView().m_int2);

   EXPECT_EQ(3, otherMutableView().m_int1);
   EXPECT_EQ(4, otherMutableView().m_int2);

   mutableView().m_int1 = 42;
   mutableView().m_int2 = 43;

   EXPECT_EQ(42, obj.m_int1);
   EXPECT_EQ(43, obj.m_int2);

   EXPECT_EQ(42, mutableView().m_int1);
   EXPECT_EQ(43, mutableView().m_int2);

   EXPECT_EQ(42, otherMutableView().m_int1);
   EXPECT_EQ(43, otherMutableView().m_int2);

   mutableView().m_int1 = 52;
   mutableView().m_int2 = 53;

   EXPECT_EQ(52, obj.m_int1);
   EXPECT_EQ(53, obj.m_int2);

   EXPECT_EQ(52, mutableView().m_int1);
   EXPECT_EQ(53, mutableView().m_int2);

   EXPECT_EQ(52, otherMutableView().m_int1);
   EXPECT_EQ(53, otherMutableView().m_int2);
}

TEST_F(ViewFixture, make_const)
{
   mutableView().m_int1 = 42;
   ConstView localConstView1{ mutableView.makeConstView() };
   ConstView localConstView2{ otherMutableView.makeConstView() };
   ConstView localConstView3{ constView.makeConstView() };
   ConstView localConstView4{ otherConstView.makeConstView() };

   EXPECT_EQ(42, localConstView1().m_int1);
   EXPECT_EQ(42, localConstView2().m_int1);
   EXPECT_EQ(42, localConstView3().m_int1);
   EXPECT_EQ(42, localConstView4().m_int1);

   // does not compile (good!), because makeConstView() returns a const nonref and
   // OtherConstView is const ref
   // OtherConstView localConstView_{ mutableView.makeConstView() };
}


} // namespace test
