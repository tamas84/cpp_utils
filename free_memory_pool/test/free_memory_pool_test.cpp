#include <gtest/gtest.h>
#include <free_memory_pool.h>

#include <array>
#include <cstdint>
#include <memory>
#include <string>

namespace
{
static uint32_t numCallbackCalled{ 0 };

auto loggerCallback = [&numCalled = numCallbackCalled](const std::string&) { ++numCalled; };

struct TestType
{
   TestType() = default;
   TestType(int32_t value) : m_value(value) {}
   int32_t m_value{ 0 };
};
} // namespace

namespace test
{

using namespace utils;

class FreeMemoryPoolTestFixture : public ::testing::Test
{
public:
   FreeMemoryPoolTestFixture()
      : m_pool(m_maxPoolSize)
   {
   }

protected:
   constexpr static size_t m_maxPoolSize = 1024U;
   FreeMemoryPool m_pool;
};

TEST_F(FreeMemoryPoolTestFixture, test_getMaxSize)
{
   EXPECT_EQ(1024U, m_pool.getMaxSize());
}

TEST_F(FreeMemoryPoolTestFixture, test_allocate_zero_bytes)
{
   EXPECT_EQ(nullptr, m_pool.allocate(0));
}

TEST_F(FreeMemoryPoolTestFixture, test_with_logger_allocate_zero_bytes)
{
   auto numErrorBeforeTest = numCallbackCalled;
   EXPECT_EQ(nullptr, m_pool.allocate(0, loggerCallback));
   auto numErrorAfterTest = numCallbackCalled;
   EXPECT_GT(numErrorAfterTest, numErrorBeforeTest);
}

TEST_F(FreeMemoryPoolTestFixture, test_allocate_whole_memory_block)
{
   EXPECT_NE(nullptr, m_pool.allocate(m_maxPoolSize));
}

TEST_F(FreeMemoryPoolTestFixture, test_with_logger_allocate_whole_memory_block)
{
   auto numErrorBeforeTest = numCallbackCalled;
   EXPECT_NE(nullptr, m_pool.allocate(m_maxPoolSize, loggerCallback));
   auto numErrorAfterTest = numCallbackCalled;
   EXPECT_EQ(numErrorAfterTest, numErrorBeforeTest);
}

TEST_F(FreeMemoryPoolTestFixture, test_allocate_more_than_available)
{
   EXPECT_EQ(nullptr, m_pool.allocate(m_maxPoolSize + 1));
}

TEST_F(FreeMemoryPoolTestFixture, test_with_logger_allocate_more_than_available)
{
   auto numErrorBeforeTest = numCallbackCalled;
   EXPECT_EQ(nullptr, m_pool.allocate(m_maxPoolSize + 1, loggerCallback));
   auto numErrorAfterTest = numCallbackCalled;
   EXPECT_GT(numErrorAfterTest, numErrorBeforeTest);
}

TEST_F(FreeMemoryPoolTestFixture, test_allocate_smaller_chunks)
{
   constexpr size_t numSuccessfulAllocations = 4;
   constexpr size_t chunkSize = m_maxPoolSize / numSuccessfulAllocations;
   std::array<uint8_t*, numSuccessfulAllocations> memAddresses;

   for (int i = 0; i < numSuccessfulAllocations; ++i)
   {
      uint8_t* address = static_cast<uint8_t*>(m_pool.allocate(chunkSize));
      EXPECT_NE(nullptr, address);
      memAddresses[i] = address;
   }

   int* address = static_cast<int*>(m_pool.allocate(1));
   EXPECT_EQ(nullptr, address);

   for (int i = 0; i < numSuccessfulAllocations - 1; ++i)
   {
      auto a = memAddresses[i] - memAddresses[i + 1];
      auto b = memAddresses[i + 1] - memAddresses[i];
      EXPECT_LT(memAddresses[i], memAddresses[i + 1]);
      EXPECT_EQ(std::abs(memAddresses[i + 1] - memAddresses[i]), chunkSize);
   }
}

TEST_F(FreeMemoryPoolTestFixture, test_resetPool)
{
   constexpr size_t newSize{ 512U };
   m_pool.resetPool(newSize);
   EXPECT_EQ(newSize, m_pool.getMaxSize());
}

TEST_F(FreeMemoryPoolTestFixture, test_resetPool_allocate_zero_bytes)
{
   constexpr size_t newSize{ 0U };
   m_pool.resetPool(newSize);
   EXPECT_EQ(m_maxPoolSize, m_pool.getMaxSize());
}

TEST_F(FreeMemoryPoolTestFixture, test_with_logger_resetPool_allocate_zero_bytes)
{
   auto numErrorBeforeTest = numCallbackCalled;
   constexpr size_t newSize{ 0U };
   m_pool.resetPool(newSize, loggerCallback);
   auto numErrorAfterTest = numCallbackCalled;
   EXPECT_EQ(m_maxPoolSize, m_pool.getMaxSize());
   EXPECT_GT(numErrorAfterTest, numErrorBeforeTest);
}

TEST_F(FreeMemoryPoolTestFixture, test_with_type)
{
   void* memory = m_pool.allocate(sizeof(TestType));
   TestType* obj1 = new(memory) TestType(5);
   EXPECT_EQ(5, obj1->m_value);
}

} // namespace test
