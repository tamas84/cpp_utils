#include <cstdlib>
#include <memory>

namespace utils
{
namespace detail
{

struct DummyErrorCallback
{
   void operator()(const std::string&) const {}
};

} // namespace detail

class FreeMemoryPool
{
public:
   template <typename ErrorCallbackT = detail::DummyErrorCallback>
   FreeMemoryPool(size_t size, const ErrorCallbackT& logger = detail::DummyErrorCallback{})
      : m_memory{ std::make_unique<uint8_t[]>(size)}
      , m_next(m_memory.get())
      , m_size(size)
   {
      if (size == 0)
      {
         logger("Can not allocate 0 bytes.");
      }
   }

   template <typename ErrorCallbackT = detail::DummyErrorCallback>
   void resetPool(size_t size, const ErrorCallbackT& logger = detail::DummyErrorCallback{})
   {
      if (size == 0)
      {
         logger("Can not allocate 0 bytes.");
         return;
      }

      m_memory.reset(new uint8_t[size]);
      m_next = m_memory.get();
      m_size = size;
   }

   template <typename ErrorCallbackT = detail::DummyErrorCallback>
   void* allocate(size_t size, const ErrorCallbackT& logger = detail::DummyErrorCallback{})
   {
      if (size == 0)
      {
         logger("Can not allocate 0 bytes.");
         return nullptr;
      }

      if (m_next + size > m_memory.get() + m_size)
      {
         logger("There is not enough free space in the pool!");
         return nullptr;
      }

      auto allocPtr = m_next;
      m_next = m_next + size;
      return allocPtr;

   }

   void deallocateAll()
   {
      m_next = m_memory.get();
   }

   size_t getMaxSize() { return m_size; }

private:
   std::unique_ptr<uint8_t[]> m_memory;
   uint8_t* m_next{ nullptr };
   size_t m_size{ 0U };
};

} // namespace utils
