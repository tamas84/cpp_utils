//Copyright (c) <2021> <Tamas Gergely>

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this file (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.


#pragma once


//// TODOs
// Better iterator (random access iterator)
// Better / more UTs
////

#include <cmath>
#include <cstddef>
#include <cstdint>
#include <cstdlib>
#include <iterator>
#include <limits>

namespace
{
using SizeType = size_t;

template<typename T>
concept IntegerType = std::is_integral<T>::value;

template<SizeType value>
concept PowerOfTwo =
      IntegerType<SizeType> &&
      (value & (value-1)) == 0;
}


template <typename T, SizeType CAPACITY>
   requires PowerOfTwo<CAPACITY>
class CircularBuffer
{
public:
   struct Iterator
   {
      using iterator_category = std::bidirectional_iterator_tag;
      using difference_type   = std::ptrdiff_t;
      using value_type        = T;
      using pointer           = T*;
      using reference         = T&;

      Iterator(CircularBuffer<T, CAPACITY>* container, bool end) : m_container(container)
      {
         if (end)
         {
            m_index = container->m_last;
            if (container->m_last <= container->m_first)
            {
               m_wrapped = true;
            }
         }
         else
         {
            m_index = container->m_first;
         }
      }

      T& operator*() const { return (*m_container)[m_index]; }

      T* operator->() { return &(*m_container)[m_index]; }

      Iterator& operator++()
      {
         m_index++;
         if (m_index == CAPACITY)
         {
            m_index = 0;
            m_wrapped = true;
         }
         return *this;
      }

      Iterator operator++(int)
      {
         Iterator tmp = *this;
         ++(*this);
         return tmp;
      }

      Iterator& operator--()
      {
         if (--m_index == std::numeric_limits<SizeType>::max())
         {
            m_index = CAPACITY - 1;
            m_wrapped = false;
         }
         return *this;
      }

      Iterator operator--(int)
      {
         Iterator tmp = *this;
         --(*this);
         return tmp;
      }

      friend bool operator== (const Iterator& a, const Iterator& b)
      {
         return a.m_wrapped == b.m_wrapped &&
               a.m_index == b.m_index;
      };

      friend bool operator!= (const Iterator& a, const Iterator& b)
      {
         return a.m_wrapped != b.m_wrapped  ||
               a.m_index != b.m_index;
      };

   private:
      SizeType m_index;
      bool m_wrapped = false;
      CircularBuffer<T, CAPACITY>* m_container;
   };

   Iterator begin()
   {
      return Iterator(this, isEmpty() ? true : false);
   }

   Iterator end()
   {
      return Iterator(this, true);
   }

public:
   CircularBuffer()
   {
      m_buffer = static_cast<T*>(::operator new(sizeof(T) * CAPACITY));
   }

   CircularBuffer(const CircularBuffer& other)
      : m_buffer(static_cast<T*>(::operator new(sizeof(T) * CAPACITY))),
        m_first(other.m_first),
        m_last(other.m_last),
        m_isFull(other.m_isFull)
   {
      for (SizeType i = 0; i < other.getSize(); ++i)
      {
         m_buffer[i] = other.m_buffer[i];
      }
   }

   CircularBuffer(const CircularBuffer& other) requires std::is_trivially_copyable<T>::value
      : m_buffer(static_cast<T*>(::operator new(sizeof(T) * CAPACITY))),
        m_first(other.m_first),
        m_last(other.m_last),
        m_isFull(other.m_isFull)
   {
      memcpy(m_buffer, other.m_buffer, sizeof(T) * CAPACITY);
   }

   CircularBuffer(CircularBuffer&& other) noexcept
   {
      std::swap(m_buffer, other.m_buffer);
      std::swap(m_first, other.m_first);
      std::swap(m_last, other.m_last);
      std::swap(m_isFull, other.m_isFull);
   }

   CircularBuffer& operator=(CircularBuffer other)
   {
      swap(*this, other);
      return *this;
   }

   ~CircularBuffer()
   {
      callDtors();
      ::operator delete(m_buffer);
   }

   void callDtors()
   {
      if (isEmpty())
      {
         return;
      }

      if (isFull())
      {
         for (SizeType i = 0; i < CAPACITY; ++i)
         {
            m_buffer[i].~T();
         }
      }
      else
      {
         if (m_first < m_last)
         {
            for (SizeType i = m_first; i < m_last; ++i)
            {
               m_buffer[i].~T();
            }
         }

         if (m_last < m_first)
         {
            for (SizeType i = m_first; i < CAPACITY; ++i)
            {
               m_buffer[i].~T();
            }
            for (SizeType i = 0; i < m_last; ++i)
            {
               m_buffer[i].~T();
            }
         }
      }
   }

   void pushBack(const T& element)
   {
      if (m_isFull)
      {
         m_buffer[m_first++].~T();
         wrapAround(m_first);
      }

      m_buffer[m_last++] = element;
      wrapAround(m_last);

      if (m_first == m_last)
      {
         m_isFull = true;
      }
   }

   template<typename ...Args>
   void emplaceBack(Args&&... args)
   {
      if (m_isFull)
      {
         m_buffer[m_first++].~T();
         wrapAround(m_first);
      }

      new (m_buffer + m_last) T(std::forward<Args>(args)...);
      ++m_last;
      wrapAround(m_last);

      if (m_first == m_last)
      {
         m_isFull = true;
      }
   }

   const T& front() const
   {
      // undefined if empty
      return m_buffer[m_first];
   }

   void popFront()
   {
      // undefined if empty
      m_isFull = false;
      m_buffer[m_first++].~T();
      wrapAround(m_first);
   }

   T& operator[](SizeType index)
   {
      SizeType i = (m_first + index) % CAPACITY;
      return m_buffer[i];
   }

   const T& operator[](SizeType index) const
   {
      SizeType i = m_first + index;
      wrapAround(i);
      return m_buffer[i];
   }

   void clear()
   {
      m_isFull = false;
      for (SizeType i = 0; i < getSize(); ++i)
      {
         m_buffer[i].~T();
         m_first++;
         wrapAround(m_last);
      }
   }

   SizeType getCapacity() const { return CAPACITY; }

   bool isEmpty() const { return !m_isFull && m_first == m_last; }

   bool isFull() const { return m_isFull; }

   SizeType getSize() const
   {
      if (isEmpty())
      {
         return 0;
      }
      else if(isFull())
      {
         return CAPACITY;
      }
      else if (m_last > m_first)
      {
         return m_last - m_first - 1;
      }
      else
      {
         return CAPACITY - (m_first - m_last);
      }
   }

private:
   friend void swap(CircularBuffer& left, CircularBuffer& right) noexcept
   {
      std::swap(left.m_buffer, right.m_buffer);
      std::swap(left.m_first, right.m_first);
      std::swap(left.m_last, right.m_last);
      std::swap(left.m_isFull, right.m_isFull);
   }

   constexpr static SizeType shiftAmount()
   {
      return std::log2(CAPACITY) + 1;
   }

   void wrapAround(SizeType& var) const
   {
      while (var >= CAPACITY)
      {
         var = (var >> shiftAmount());
      }
   }

   T* m_buffer = nullptr;
   SizeType m_first = 0;
   SizeType m_last = 0;
   bool m_isFull = false;
};
