#include <gtest/gtest.h>

#include <algorithm>
#include <atomic>
#include <iterator>
#include <memory>
#include <numeric>

#include <circular_buffer.h>


namespace test
{
std::atomic<uint32_t> g_deleteCounter{0};
std::atomic<uint32_t> g_createCounter{0};
std::atomic<uint32_t> g_copyCounter{0};
std::atomic<uint32_t> g_moveCounter{0};

template <typename T>
struct TestType
{
   explicit TestType(T value) : m_value(value)
   {
      g_createCounter++;
   }

   TestType(const TestType& other) : m_value(other.m_value)
   {
      g_createCounter++;
   }

   TestType(TestType&& other) : m_value(std::move(other.m_value))
   {
      g_createCounter++;
   }

   TestType& operator=(const TestType& other)
   {
      if (this != &other)
      {
         g_copyCounter++;
         m_value = other.m_value;
      }
      return *this;
   }

   TestType& operator=(TestType&& other)
   {
      if (this != &other)
      {
         g_moveCounter++;
         m_value = other.m_value;
      }
      return *this;
   }

   ~TestType()
   {
      g_deleteCounter++;
   }

   T operator()() const
   {
      return m_value;
   }

private:
   T m_value;
};

template <typename T>
struct TestType2
{
   TestType2(T value) : m_value(value)
   {
      g_createCounter++;
   }

   T operator()() const
   {
      return m_value;
   }

private:
   T m_value;
};



class CircularBufferTestFixture : public ::testing::Test
{
protected:
   constexpr static size_t CAPACITY = 8;
   virtual void SetUp()
   {
      m_emptyBuff.clear();
      m_fullBuff.clear();
      m_emptyIntBuff.clear();
      m_fullIntBuff.clear();


      for (uint8_t i = 0; i < CAPACITY; ++i)
      {
         m_fullBuff.pushBack(TestType<int>(i));
         m_fullIntBuff.pushBack(i);
      }
      g_deleteCounter = 0;
      g_createCounter = 0;
      g_copyCounter = 0;
      g_moveCounter = 0;
   }

   virtual void TearDown()
   {
   }

   CircularBuffer<TestType<int>, CAPACITY> m_emptyBuff;
   CircularBuffer<TestType<int>, CAPACITY> m_fullBuff;
   CircularBuffer<int, CAPACITY> m_emptyIntBuff;
   CircularBuffer<int, CAPACITY> m_fullIntBuff;
};

TEST_F(CircularBufferTestFixture, test_emptyBuffer_capacity)
{
   EXPECT_EQ(CAPACITY, m_emptyBuff.getCapacity());
}

TEST_F(CircularBufferTestFixture, test_emptyBuffer_clear)
{
   m_emptyBuff.clear();
   EXPECT_EQ(0, m_emptyBuff.getSize());
}

TEST_F(CircularBufferTestFixture, test_emptyBuffer_dtor)
{
   std::unique_ptr<CircularBuffer<TestType<int>, CAPACITY>> buffer(new CircularBuffer<TestType<int>, CAPACITY>());
   buffer.reset(nullptr);
   EXPECT_EQ(0, g_deleteCounter);
}

TEST_F(CircularBufferTestFixture, test_emptyBuffer_isEmpty_isFull)
{
   EXPECT_EQ(true, m_emptyBuff.isEmpty());
   EXPECT_EQ(false, m_emptyBuff.isFull());
}

TEST_F(CircularBufferTestFixture, test_emptyBuffer_front)
{
   m_emptyBuff.pushBack(TestType(5));
   EXPECT_EQ(5, m_emptyBuff.front()());
}

TEST_F(CircularBufferTestFixture, test_emptyBuffer_emplaceBack)
{
   m_emptyBuff.emplaceBack(TestType(5));
   m_emptyBuff.emplaceBack(5);
}

TEST_F(CircularBufferTestFixture, test_nonExplicit_emptyBuffer_emplaceBack)
{
   CircularBuffer<TestType2<int>, CAPACITY> buffer;
   buffer.emplaceBack(5);
   EXPECT_EQ(5, buffer.front()());
}

////////
TEST_F(CircularBufferTestFixture, test_fullBuffer_clear)
{
   m_fullBuff.clear();
   EXPECT_EQ(0, m_fullBuff.getSize());
}

TEST_F(CircularBufferTestFixture, test_fullBuffer_dtor)
{
   std::unique_ptr<CircularBuffer<TestType<int>, CAPACITY>> buffer(new CircularBuffer<TestType<int>, CAPACITY>());
   for (uint8_t i = 0; i < CAPACITY; ++i)
   {
      buffer->pushBack(TestType<int>(i));
   }
   g_deleteCounter = 0;
   buffer.reset(nullptr);
   EXPECT_EQ(CAPACITY, g_deleteCounter);
}

TEST_F(CircularBufferTestFixture, test_fullBuffer_isEmpty_isFull)
{
   EXPECT_EQ(false, m_fullBuff.isEmpty());
   EXPECT_EQ(true, m_fullBuff.isFull());
}

TEST_F(CircularBufferTestFixture, test_fullBuffer_accessElements)
{
   EXPECT_EQ(CAPACITY, m_fullBuff.getCapacity());
   for (uint8_t i = 0; i < m_fullBuff.getCapacity(); ++i)
   {
      EXPECT_EQ(i, m_fullBuff[i]());
   }
}

TEST_F(CircularBufferTestFixture, test_fullBuffer_pushBack_accessElements_delete)
{
   std::unique_ptr<CircularBuffer<TestType<size_t>, CAPACITY>> buffer(new CircularBuffer<TestType<size_t>, CAPACITY>());
   for (uint8_t i = 0; i < CAPACITY; ++i)
   {
      buffer->pushBack(TestType<size_t>(i));
   }

   g_deleteCounter = 0;
   buffer->pushBack(TestType(CAPACITY));      // increases g_deleteCounter by 2
   buffer->pushBack(TestType(CAPACITY + 1));  // increases g_deleteCounter by 2
   buffer->pushBack(TestType(CAPACITY + 2));  // increases g_deleteCounter by 2

   for (uint8_t i = 0; i < buffer->getCapacity(); ++i)
   {
      EXPECT_EQ(i + 3, (*buffer)[i]());
   }

   buffer.reset(nullptr);
   constexpr size_t tempDeleted = 3;
   constexpr size_t aboveCapacity = 3;
   EXPECT_EQ(CAPACITY + tempDeleted + aboveCapacity, g_deleteCounter);
}

TEST_F(CircularBufferTestFixture, test_fullBuffer_emplaceBack_accessElements_delete)
{
   std::unique_ptr<CircularBuffer<TestType<size_t>, CAPACITY>> buffer(new CircularBuffer<TestType<size_t>, CAPACITY>());
   for (uint8_t i = 0; i < CAPACITY; ++i)
   {
      buffer->emplaceBack(i);
   }

   g_deleteCounter = 0;
   buffer->emplaceBack(CAPACITY);
   buffer->emplaceBack(CAPACITY + 1);
   buffer->emplaceBack(CAPACITY + 2);

   for (uint8_t i = 0; i < buffer->getCapacity(); ++i)
   {
      EXPECT_EQ(i + 3, (*buffer)[i]());
   }

   buffer.reset(nullptr);
   constexpr size_t aboveCapacity = 3;
   EXPECT_EQ(CAPACITY + aboveCapacity, g_deleteCounter);
}

TEST_F(CircularBufferTestFixture, test_fullBuffer_pushBack_isEmpty_isFull)
{
   EXPECT_EQ(CAPACITY, m_fullBuff.getCapacity());

   m_fullBuff.pushBack(TestType(42));
   m_fullBuff.pushBack(TestType(43));
   m_fullBuff.pushBack(TestType(44));

   EXPECT_EQ(false, m_fullBuff.isEmpty());
   EXPECT_EQ(true, m_fullBuff.isFull());
}

TEST_F(CircularBufferTestFixture, test_fullBuffer_popFront)
{
   for (uint32_t i = 0; i < 4; ++i)
   {
      m_fullBuff.popFront();
   }
   EXPECT_EQ(false, m_fullBuff.isEmpty());
   EXPECT_EQ(false, m_fullBuff.isFull());

   EXPECT_EQ(4, g_deleteCounter);
}

TEST_F(CircularBufferTestFixture, test_fullBuffer_cpyCtor)
{
   auto fullBuffer = m_fullBuff;
   EXPECT_EQ(CAPACITY, fullBuffer.getCapacity());
   EXPECT_EQ(CAPACITY, fullBuffer.getSize());
   for (uint32_t i = 0; i < CAPACITY; i++)
   {
      EXPECT_EQ(i, fullBuffer[i]());
   }
}

TEST_F(CircularBufferTestFixture, test_fullBuffer_cpyCtor_cpyAssignment)
{
   auto fullBuffer = m_fullBuff; // cpy ctor
   EXPECT_EQ(CAPACITY, fullBuffer.getCapacity());
   EXPECT_EQ(CAPACITY, fullBuffer.getSize());
   for (uint32_t i = 0; i < CAPACITY; i++)
   {
      EXPECT_EQ(i, fullBuffer[i]());
   }

   EXPECT_EQ(0, g_deleteCounter);
   EXPECT_EQ(CAPACITY, g_copyCounter);
   fullBuffer[0] = TestType<int>(55);
   EXPECT_EQ(55, fullBuffer[0]());
   EXPECT_EQ(0, m_fullBuff[0]());

   g_deleteCounter = 0;
   g_copyCounter = 0;

   fullBuffer = m_fullBuff; // cpy assignment op
   EXPECT_EQ(CAPACITY, g_deleteCounter);
   EXPECT_EQ(CAPACITY, g_copyCounter);
   EXPECT_EQ(0, fullBuffer[0]());
}

TEST_F(CircularBufferTestFixture, test_fullBuffer_moveCtor_moveAassigment)
{
   auto fullBuffer = std::move(m_fullBuff); // move ctor
   EXPECT_EQ(CAPACITY, fullBuffer.getCapacity());
   EXPECT_EQ(CAPACITY, fullBuffer.getSize());
   for (uint32_t i = 0; i < CAPACITY; i++)
   {
      EXPECT_EQ(i, fullBuffer[i]());
   }

   EXPECT_EQ(0, g_deleteCounter);
   EXPECT_EQ(0, g_moveCounter);  // swap was used, no move ctor was called for TestType
   EXPECT_EQ(CAPACITY, fullBuffer.getCapacity());
   EXPECT_EQ(true, fullBuffer.isFull());

   fullBuffer = std::move(m_emptyBuff);
   EXPECT_EQ(CAPACITY, fullBuffer.getCapacity());
   EXPECT_EQ(false, fullBuffer.isFull());
   EXPECT_EQ(true, fullBuffer.isEmpty());

}


/////
///// Iterator test
/////


TEST_F(CircularBufferTestFixture, test_emptyIntBuffer_iterator)
{
   EXPECT_EQ(CAPACITY, m_emptyIntBuff.getCapacity());
   auto sum = std::accumulate(m_emptyIntBuff.begin(), m_emptyIntBuff.end(), 0);
   EXPECT_EQ(0, sum);
}

TEST_F(CircularBufferTestFixture, test_fullIntBuffer_iterator_accumulate)
{
   EXPECT_EQ(CAPACITY, m_fullIntBuff.getCapacity());
   auto sum = std::accumulate(m_fullIntBuff.begin(), m_fullIntBuff.end(), 0);
   EXPECT_EQ(28, sum);
}

TEST_F(CircularBufferTestFixture, test_fullIntBuffer_iterator_fill)
{
   EXPECT_EQ(CAPACITY, m_fullIntBuff.getCapacity());
   std::fill(m_fullIntBuff.begin(), m_fullIntBuff.end(), 5);
   for (auto &elem : m_fullIntBuff)
   {
      EXPECT_EQ(5, elem);
   }
}

TEST_F(CircularBufferTestFixture, test_fullIntBuffer_pushBack_iterator)
{
   EXPECT_EQ(CAPACITY, m_fullIntBuff.getCapacity());
   m_fullIntBuff.pushBack(CAPACITY);
   m_fullIntBuff.pushBack(CAPACITY + 1);
   auto sum = std::accumulate(m_fullIntBuff.begin(), m_fullIntBuff.end(), 0);
   EXPECT_EQ(44, sum);
}

TEST_F(CircularBufferTestFixture, test_fullIntBuffer_iterator_advance_pos)
{
   EXPECT_EQ(CAPACITY, m_fullIntBuff.getCapacity());
   auto it = m_fullIntBuff.begin();
   EXPECT_EQ(0, *it);
   std::advance(it, 1);
   EXPECT_EQ(1, *it);
}

TEST_F(CircularBufferTestFixture, test_fullIntBuffer_iterator_advance_neg)
{
   EXPECT_EQ(CAPACITY, m_fullIntBuff.getCapacity());
   auto it = m_fullIntBuff.begin();
   EXPECT_EQ(0, *it);
   std::advance(it, -1);
   EXPECT_EQ(7, *it);
   std::advance(it, -1);
   EXPECT_EQ(6, *it);
   std::advance(it, -4);
   EXPECT_EQ(2, *it);
   std::advance(it, -2);
   EXPECT_EQ(0, *it);
   std::advance(it, -4);
   EXPECT_EQ(4, *it);
}

TEST_F(CircularBufferTestFixture, test_fullIntBuffer_iterator_deref_pos)
{
   EXPECT_EQ(CAPACITY, m_fullIntBuff.getCapacity());
   auto it = m_fullIntBuff.begin();
   EXPECT_EQ(0, *it++);
   EXPECT_EQ(1, *it++);
}

TEST_F(CircularBufferTestFixture, test_fullIntBuffer_iterator_deref_neg)
{
   EXPECT_EQ(CAPACITY, m_fullIntBuff.getCapacity());
   auto it = m_fullIntBuff.begin();
   EXPECT_EQ(0, *it--);
   EXPECT_EQ(CAPACITY - 1, *it--);
}

} // namespace test
