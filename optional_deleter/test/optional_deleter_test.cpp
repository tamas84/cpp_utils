#include <gtest/gtest.h>

#include <optional_deleter.h>

#include <atomic>


namespace test
{

static std::atomic<int> global = 0;

struct MyType
{
   ~MyType()
   {
      global++;
   }
};



TEST(OptionalDeleterTest, basicTest)
{
   auto ptr1 = std::make_unique<MyType>();
   ptr1.reset();
   EXPECT_EQ(1, global);
   auto ptr2 = std::unique_ptr<MyType, OptionalDeleter<MyType>>(new MyType(), OptionalDeleter<MyType>(true));
   ptr2.reset();
   EXPECT_EQ(2, global);
   auto ptr3 = std::unique_ptr<MyType, OptionalDeleter<MyType>>(new MyType(), OptionalDeleter<MyType>(false));
   ptr3.reset();
   EXPECT_EQ(2, global);
}

} // namespace test
