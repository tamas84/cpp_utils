#ifndef OPTIONAL_DELETER_H
#define OPTIONAL_DELETER_H

template<class T>
struct OptionalDeleter
{
   bool m_delete;

   OptionalDeleter(bool del = true)
      :  m_delete(del)
   {
   }

   void operator()(T* ptr) const
   {
      if (m_delete)
      {
         delete ptr;
      }
   }
};


#endif // OPTIONAL_DELETER_H
