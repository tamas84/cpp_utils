#include <gtest/gtest.h>
#include <scope_guard.h>

#include <cstring> // uint32_t
#include <utility> // declval

namespace
{
   uint32_t& getCounter()
   {
      static uint32_t counter{ 0 };
      return counter;
   }
} // namespace

namespace test
{

using namespace utils;

class ScopeGuardTestFixture : public ::testing::Test
{
public:
   ScopeGuardTestFixture()
   {
   }

protected:
   void SetUp() override
   {
      m_counterBegin = getCounter();
   }

   uint32_t m_counterBegin{ 0 };
};

TEST_F(ScopeGuardTestFixture, test_basic)
{
   {
      ScopeGuard _([]() { ++getCounter(); });
   }

   EXPECT_EQ(m_counterBegin + 1, getCounter());
}

TEST_F(ScopeGuardTestFixture, test_setActive_true)
{
   {
      ScopeGuard guard([]() { ++getCounter(); });
      guard.setActive(true);
   }

   EXPECT_EQ(m_counterBegin + 1, getCounter());
}

TEST_F(ScopeGuardTestFixture, test_setActive_false)
{
   {
      ScopeGuard guard([]() { ++getCounter(); });
      guard.setActive(false);
   }

   EXPECT_EQ(m_counterBegin, getCounter());
}

TEST_F(ScopeGuardTestFixture, test_move)
{
   {
      ScopeGuard guard([]() { ++getCounter(); });
      {
         auto guard2{ std::move(guard) };
      }
      EXPECT_EQ(m_counterBegin + 1, getCounter());
   }

   // `guard`'s dtor doesn't increase the counter
   EXPECT_EQ(m_counterBegin + 1, getCounter());
}

} // namespace test
