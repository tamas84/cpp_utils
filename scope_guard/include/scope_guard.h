#pragma once

#include <concepts>

namespace utils
{

template <typename F>
   requires std::invocable<F>
class ScopeGuard
{
public:
   explicit ScopeGuard(F func)
      : m_func{ std::move(func) }
   {}

   ScopeGuard(ScopeGuard&& other)
      : m_func{ std::forward<F>(other.m_func) }
      , m_isActive{ std::exchange(other.m_isActive, false) }
   {
   }

   ScopeGuard(const ScopeGuard&) = delete;
   ScopeGuard& operator=(const ScopeGuard&) = delete;
   ScopeGuard& operator=(ScopeGuard&&) = delete;

   ~ScopeGuard()
   {
      if (m_isActive)
      {
         m_func();
      }
   }

   void setActive(const bool active)
   {
      m_isActive = active;
   }

private:
   F m_func;
   bool m_isActive{ true };
};

} // namespace utils
