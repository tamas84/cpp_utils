#include <realtime_logger.h>

int main()
{
   using namespace utils;

   {
      constexpr bool isActive = true;
      RealtimeLogger<isActive, int, double, std::string> logger("file.csv", {"int_header", "double_header", "string_header"});
      logger.write(1, 2.2, "stringA");
      logger.write(2, 3.3, "stringB");
      logger.writeAndFlush(3, 4.4, "stringC");

      auto logger2 = std::move(logger);
      logger2.write(33, 44.4, "stringD");

   }

   {
      constexpr bool isActive = false;
      RealtimeLogger<isActive, int, double, std::string> voidLogger("file2.csv", {"int_header", "double_header", "string_header"});
      voidLogger.writeAndFlush(3, 4.4, "stringC");
   }
}
