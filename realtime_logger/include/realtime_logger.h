#pragma once

#include <initializer_list>
#include <fstream>
#include <sstream>
#include <string_view>

namespace utils
{

template<bool, typename ...ColumnTypes>
class RealtimeLogger
{
   static_assert(sizeof...(ColumnTypes) > 0);

public:
   RealtimeLogger(std::string_view /*fileName*/,
                  std::initializer_list<std::string_view> /*columnNames*/)
   {
   }

   void write(ColumnTypes&&... /*data*/)
   {
   }

   void writeAndFlush(ColumnTypes&&... /*data*/)
   {
   }
};

template<typename ...ColumnTypes>
class RealtimeLogger <true, ColumnTypes...>
{
   static_assert(sizeof...(ColumnTypes) > 0);

public:
   RealtimeLogger(std::string_view fileName,
                  std::initializer_list<std::string_view> columnNames)
      : m_file(fileName.data())
   {
      std::stringstream header;
      for (std::string_view columnName : columnNames)
      {
         header << columnName << ",";
      }
      header.seekp(-1, std::ios_base::end);
      header << "\n";
      m_file << header.str();
      m_file.flush();
   }

   RealtimeLogger(RealtimeLogger&&) = default;
   RealtimeLogger(const RealtimeLogger&) = delete;

   ~RealtimeLogger()
   {
      m_file.close();
   }

   void write(ColumnTypes&&... data)
   {
      size_t index = 0;
      (storeValue(index, std::forward<ColumnTypes>(data)), ...);
      m_file << "\n";
   }

   void writeAndFlush(ColumnTypes&&... data)
   {
      size_t index = 0;
      (storeValue(index, std::forward<ColumnTypes>(data)), ...);
      m_file << "\n";
      m_file.flush();
   }

private:
   template<typename T>
   void storeValue(size_t& index, T&& arg)
   {
      m_file << arg;

      if (++index < sizeof... (ColumnTypes))
      {
         m_file << ",";
      }
   }

   std::ofstream m_file;
};

} // namespace utils
