list(APPEND CMAKE_MODULE_PATH ${CMAKE_BINARY_DIR})
list(APPEND CMAKE_PREFIX_PATH ${CMAKE_BINARY_DIR})

if(NOT EXISTS "${CMAKE_BINARY_DIR}/conan.cmake")
   message(STATUS "Downloading conan.cmake from https://github.com/conan-io/cmake-conan")
   file(
      DOWNLOAD "https://raw.githubusercontent.com/conan-io/cmake-conan/0.18.1/conan.cmake"
                  "${CMAKE_BINARY_DIR}/conan.cmake"
                  EXPECTED_HASH SHA256=5cdb3042632da3efff558924eecefd580a0e786863a857ca097c3d1d43df5dcd
                  TLS_VERIFY ON)
endif()

include(${CMAKE_BINARY_DIR}/conan.cmake)

if(NOT WIN32)
   conan_cmake_autodetect(settings BUILD_TYPE ${dependencyBuildType})
else()
   conan_cmake_autodetect(settings)
endif()
#message(STATUS "--------- settings: ${settings}")
conan_cmake_install(
                     PATH_OR_REFERENCE ${CMAKE_SOURCE_DIR}
                     BUILD missing
                     REMOTE conancenter
                     SETTINGS ${settings})

#message(STATUS "------------ cpputils: jsoncpp_FOUND ${jsoncpp_FOUND}")
#message(STATUS "------------ cpputils: jsoncpp_INCLUDE_DIR ${jsoncpp_INCLUDE_DIR}")
#message(STATUS "------------ cpputils: glm_FOUND ${glm_FOUND}")
#message(STATUS "------------ cpputils: glm_INCLUDE_DIR: ${glm_INCLUDE_DIR}")
