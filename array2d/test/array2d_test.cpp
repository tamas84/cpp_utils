#include <gtest/gtest.h>
#include <array2d.h>

#include <cstdint>
#include <memory>


namespace test
{

using namespace utils;

class Array2DTestFixture : public ::testing::Test
{
public:
   Array2DTestFixture()
      : m_floatBuffer([this]() {
           std::unique_ptr<float[]> buffer = std::make_unique<float[]>(m_floatBufferRows * m_floatBufferColumns);
           for (uint32_t row = 0; row < m_floatBufferRows; ++row)
           {
              for (uint32_t column = 0; column < m_floatBufferColumns; ++column)
              {
                 buffer[row * m_floatBufferColumns + column] = static_cast<float>(row);
              }
           }
           return buffer; }())
      , m_floatArrayInitialized(m_floatBufferRows, m_floatBufferColumns, std::move(m_floatBuffer))
      , m_floatArrayUninitialized(m_floatBufferRows, m_floatBufferColumns)
   {
   }

protected:
   constexpr static uint32_t m_floatBufferRows = 10;
   constexpr static uint32_t m_floatBufferColumns = 10;
   std::unique_ptr<float[]> m_floatBuffer = std::make_unique<float[]>(m_floatBufferRows * m_floatBufferColumns);
   std::unique_ptr<float[]> m_floatBuffer2;
   Array2D<float> m_floatArrayInitialized;
   Array2D<float> m_floatArrayUninitialized;
};


TEST_F(Array2DTestFixture, test_initialized)
{
   EXPECT_EQ(m_floatArrayInitialized.getNumRows(), m_floatBufferRows);
   EXPECT_EQ(m_floatArrayInitialized.getNumColumns(), m_floatBufferColumns);

   for (uint32_t row = 0; row < m_floatBufferRows; ++row)
   {
      for (uint32_t column = 0; column < m_floatBufferColumns; ++column)
      {
         EXPECT_FLOAT_EQ(m_floatArrayInitialized(row, column), static_cast<float>(row));
      }
   }
}

TEST_F(Array2DTestFixture, test_initialized_data)
{
   const auto* const data = m_floatArrayInitialized.data();
   for (uint32_t row = 0; row < m_floatBufferRows; ++row)
   {
      for (uint32_t column = 0; column < m_floatBufferColumns; ++column)
      {
         EXPECT_FLOAT_EQ(*(data + (row * m_floatBufferColumns) + column), static_cast<float>(row));
      }
   }
}

TEST_F(Array2DTestFixture, test_uninitialized)
{
   EXPECT_EQ(m_floatArrayUninitialized.getNumRows(), m_floatBufferRows);
   EXPECT_EQ(m_floatArrayUninitialized.getNumColumns(), m_floatBufferColumns);

   // check default
   for (uint32_t row = 0; row < m_floatBufferRows; ++row)
   {
      for (uint32_t column = 0; column < m_floatBufferColumns; ++column)
      {
         EXPECT_FLOAT_EQ(m_floatArrayUninitialized(row, column), 0.0f);
      }
   }

   // emplace
   for (uint32_t row = 0; row < m_floatBufferRows; ++row)
   {
      for (uint32_t column = 0; column < m_floatBufferColumns; ++column)
      {
         m_floatArrayUninitialized.emplace(row, column, row + 10.0f);
      }
   }

   // check emplaced
   for (uint32_t row = 0; row < m_floatBufferRows; ++row)
   {
      for (uint32_t column = 0; column < m_floatBufferColumns; ++column)
      {
         EXPECT_FLOAT_EQ(m_floatArrayUninitialized(row, column), row + 10.0f);
      }
   }

   // insert
   for (uint32_t row = 0; row < m_floatBufferRows; ++row)
   {
      for (uint32_t column = 0; column < m_floatBufferColumns; ++column)
      {
         m_floatArrayUninitialized.insert(row, column, row + 20.0f);
      }
   }

   // check inserted
   for (uint32_t row = 0; row < m_floatBufferRows; ++row)
   {
      for (uint32_t column = 0; column < m_floatBufferColumns; ++column)
      {
         EXPECT_FLOAT_EQ(m_floatArrayUninitialized(row, column), row + 20.0f);
      }
   }
}

TEST_F(Array2DTestFixture, test_initialized_move)
{
   std::unique_ptr<float[]> otherBuffer = std::make_unique<float[]>(m_floatBufferRows * m_floatBufferColumns);
   Array2D<float> otherArray(m_floatBufferRows, m_floatBufferColumns, std::move(otherBuffer));
   otherArray = std::move(m_floatArrayInitialized);

   EXPECT_EQ(otherArray.getNumRows(), m_floatBufferRows);
   EXPECT_EQ(otherArray.getNumColumns(), m_floatBufferColumns);

   for (uint32_t row = 0; row < m_floatBufferRows; ++row)
   {
      for (uint32_t column = 0; column < m_floatBufferColumns; ++column)
      {
         EXPECT_FLOAT_EQ(otherArray(row, column), static_cast<float>(row));
      }
   }
}

} // namespace test
