#include <memory>
#include <cassert>
#include <cstdint>

namespace utils
{

template <typename T>
class Array2D
{
public:
   Array2D(uint32_t rows, uint32_t columns)
      : m_rows(rows)
      , m_columns(columns)
      , m_buffer(std::make_unique<T[]>(rows * columns))
   {
      assert(rows > 0);
      assert(columns > 0);
   }

   Array2D(uint32_t rows, uint32_t columns, std::unique_ptr<T[]> buffer)
      : m_rows(rows)
      , m_columns(columns)
      , m_buffer(std::move(buffer))
   {
      assert(rows > 0);
      assert(columns > 0);
   }

   Array2D(const Array2D&) = delete;
   Array2D(Array2D&&) noexcept = default;

   Array2D& operator=(const Array2D&) = delete;
   Array2D& operator=(Array2D&&) noexcept = default;

   uint32_t getNumRows() const { return m_rows; }
   uint32_t getNumColumns() const { return m_columns; }

   T& operator()(uint32_t row, uint32_t column)
   {
      return m_buffer[m_columns * row + column];
   }

   template<typename ...Args>
   void emplace(uint32_t row, uint32_t column, Args&&... args)
   {
      operator()(row, column) = T(std::forward<Args>(args)...);
   }

   void insert(uint32_t row, uint32_t column, T value)
   {
      operator()(row, column) = std::move(value);
   }

   const T* data() const { return m_buffer.get(); }
   T* data() { return m_buffer.get(); }

private:
   uint32_t m_rows;
   uint32_t m_columns;
   std::unique_ptr<T[]> m_buffer;
};

} // namespace utils
