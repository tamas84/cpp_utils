#pragma once

namespace utils
{
namespace detail
{

struct EmptyLogCallback
{
   template <typename... Args>
   void operator()(Args&& ...args) const {}
};

} //namespace detail

template <typename LogCallbackT, typename... Args>
concept LoggerCbConcept = requires(LogCallbackT cb, Args&& ...args) {
   { cb.operator()(args...) } -> std::same_as<void>;
};

using EmptyLogCallback = detail::EmptyLogCallback;

template <LoggerCbConcept FatalCallbackT = detail::EmptyLogCallback,
          LoggerCbConcept ErrorCallbackT = detail::EmptyLogCallback,
          LoggerCbConcept WarningCallbackT = detail::EmptyLogCallback,
          LoggerCbConcept InfoCallbackT = detail::EmptyLogCallback,
          LoggerCbConcept DebugCallbackT = detail::EmptyLogCallback>
struct LogCallbacks
{
   FatalCallbackT fatalCallback;
   ErrorCallbackT errorCallback;
   WarningCallbackT warningCallback;
   InfoCallbackT infoCallback;
   DebugCallbackT debugCallback;
};

template <typename CallbacksT>
concept LoggerCallbacksConcept = requires(CallbacksT cbs) {
   { cbs.fatalCallback };
   { LoggerCbConcept<decltype(cbs.fatalCallback)> };
   { cbs.errorCallback };
   { LoggerCbConcept<decltype(cbs.errorCallback)> };
   { cbs.warningCallback };
   { LoggerCbConcept<decltype(cbs.warningCallback)> };
   { cbs.infoCallback };
   { LoggerCbConcept<decltype(cbs.infoCallback)> };
   { cbs.debugCallback };
   { LoggerCbConcept<decltype(cbs.debugCallback)> };
};

template<typename LogCallbacksT = utils::LogCallbacks<>>
   requires LoggerCallbacksConcept<LogCallbacksT>
class CppUtilsLogger
{
public:
   CppUtilsLogger(LogCallbacksT logCallbacks = LogCallbacksT{})
      : m_logCallbacks{ std::move(logCallbacks) }
   {}

   template<typename... Args>
   void logFatal(Args&&... args) const
   {
      m_logCallbacks.fatalCallback(std::forward<Args>(args)...);
   }

   template<typename... Args>
   void logError(Args&&... args) const
   {
      m_logCallbacks.errorCallback(std::forward<Args>(args)...);
   }

   template<typename... Args>
   void logWarning(Args&&... args) const
   {
      m_logCallbacks.warningCallback(std::forward<Args>(args)...);
   }

   template<typename... Args>
   void logInfo(Args&&... args) const
   {
      m_logCallbacks.infoCallback(std::forward<Args>(args)...);
   }

   template<typename... Args>
   void logDebug(Args&&... args) const
   {
      m_logCallbacks.debugCallback(std::forward<Args>(args)...);
   }

private:
   LogCallbacksT m_logCallbacks;
};

} // utils
