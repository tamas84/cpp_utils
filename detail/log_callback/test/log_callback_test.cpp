#include <gtest/gtest.h>
#include <log_callback.h>

#include <cstdint>
//#include <iostream>

namespace
{

uint32_t& getFatalCbNumCalls()
{
   static uint32_t fatalCbNumCalls{ 0 };
   return fatalCbNumCalls;
}

uint32_t& getErrorCbNumCalls()
{
   static uint32_t errorCbNumCalls{ 0 };
   return errorCbNumCalls;
}

uint32_t& getWarningCbNumCalls()
{
   static uint32_t warningCbNumCalls{ 0 };
   return warningCbNumCalls;
}

uint32_t& getInfoCbNumCalls()
{
   static uint32_t infoCbNumCalls{ 0 };
   return infoCbNumCalls;
}

uint32_t& getDebugCbNumCalls()
{
   static uint32_t debugCbNumCalls{ 0 };
   return debugCbNumCalls;
}

struct TestClientErrorLogCallback
{
   template <typename... Args>
   void operator()(Args&&... args) const
   {
      //std::cout << "error: ";
      //(std::cout << ... << std::forward<Args>(args)); std::cout << std::endl;
      getErrorCbNumCalls()++;
   }
};

struct TestClientWarningLogCallback
{
   template <typename... Args>
   void operator()(Args&&... args) const
   {
      //std::cout << "warning: ";
      //(std::cout << ... << std::forward<Args>(args)); std::cout << std::endl;
      getWarningCbNumCalls()++;
   }
};

using ClientLogCallbacks = utils::LogCallbacks<utils::EmptyLogCallback, TestClientErrorLogCallback, TestClientWarningLogCallback>;


template<typename LogCallbacksT = utils::LogCallbacks<>>
struct MyCppUnitClazz
{
   utils::CppUtilsLogger<LogCallbacksT> logger;
};

} // namespace

namespace test
{

using namespace utils;

class LogCallbackTestFixture : public ::testing::Test
{
public:
   LogCallbackTestFixture()
   {
   }
};

TEST_F(LogCallbackTestFixture, test_CppUtilsLogger_Directly)
{
   uint32_t numErrors{ getErrorCbNumCalls() };
   uint32_t numWarnings{ getWarningCbNumCalls() };
   {
      CppUtilsLogger<utils::LogCallbacks<>> logger;
      logger.logError("testError");
      logger.logWarning("testWarning");
   }

   {
      CppUtilsLogger<ClientLogCallbacks> logger;
      logger.logError("testError2");
      logger.logWarning("testWarning2");
   }

   uint32_t expectedNumWarnins{ numWarnings + 1 };
   uint32_t expectedNumErrors{ numErrors + 1 };
   EXPECT_EQ(expectedNumWarnins, getWarningCbNumCalls());
   EXPECT_EQ(expectedNumErrors, getErrorCbNumCalls());
}

TEST_F(LogCallbackTestFixture, test_Example_CppUnit_Class)
{
   uint32_t numFatals{ getFatalCbNumCalls() };
   uint32_t numErrors{ getErrorCbNumCalls() };
   uint32_t numWarnings{ getWarningCbNumCalls() };

   MyCppUnitClazz<ClientLogCallbacks> myInstance;
   myInstance.logger.logFatal("testFatal");
   myInstance.logger.logError("testError");
   myInstance.logger.logWarning("testWarning");

   uint32_t expectedNumFatals{ numFatals + 0 }; // EmptyLogCallback is used for Fatal
   uint32_t expectedNumErrors{ numErrors + 1 };
   uint32_t expectedNumWarnins{ numWarnings + 1 };

   EXPECT_EQ(expectedNumFatals, getFatalCbNumCalls());
   EXPECT_EQ(expectedNumErrors, getErrorCbNumCalls());
   EXPECT_EQ(expectedNumWarnins, getWarningCbNumCalls());
}

} // namespace test
