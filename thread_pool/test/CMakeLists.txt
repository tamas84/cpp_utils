find_package(GTest REQUIRED)

set(TEST_SRC main.cpp thread_pool_test.cpp)
add_executable(${PROJECT_NAME}_test ${TEST_SRC})

target_link_libraries(${PROJECT_NAME}_test PRIVATE
   ${PROJECT_NAME}
   $<IF:$<BOOL:CPP_UTILS_CONAN_DEPS>,GTest::gtest,${GTEST_LIBRARIES}>)
