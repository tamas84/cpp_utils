#include <gtest/gtest.h>
#include <thread_pool.h>


namespace test
{

class ThreadPoolTestFixture : public ::testing::Test
{
protected:
   float calcDouble(float f)
   {
      ++m_numCalled;
      return 2 * f;
   }

   uint32_t m_numCalled = 0;
};


TEST_F(ThreadPoolTestFixture, test_basic)
{
   std::vector<float> results;
   std::mutex mutex;
   results.reserve(2);
   utils::ThreadPool<2> pool;
   pool.addTask([this, &mutex, &results](){
      const auto result = calcDouble(1.1f);
      std::lock_guard<std::mutex> lock(mutex);
      results.push_back(std::move(result));
   });
   pool.addTask([this, &mutex, &results](){
      const auto result = calcDouble(2.2f);
      std::lock_guard<std::mutex> lock(mutex);
      results.push_back(std::move(result));
   });
   pool.processRemaining();
   std::future<void> future = pool.getFuture();
   future.get();
   ASSERT_EQ(2, results.size());
   float sum = 0;
   for (const auto& result : results)
   {
      sum += result;
   }
   EXPECT_FLOAT_EQ(6.6f, sum);
   EXPECT_EQ(2, m_numCalled);
   pool.shutdown();
}

TEST_F(ThreadPoolTestFixture, test_addTaskFailes_afterProcessInvoked)
{
   utils::ThreadPool<2> pool;
   bool addTaskResult = pool.addTask([](){
   });
   EXPECT_TRUE(addTaskResult);

   pool.processRemaining();

   addTaskResult = pool.addTask([](){
   });
   EXPECT_FALSE(addTaskResult);
   pool.shutdown();
}

TEST_F(ThreadPoolTestFixture, test_processFailes_afterProcessInvoked)
{
   utils::ThreadPool<2> pool;
   pool.addTask([](){
   });

   bool processResult = pool.processRemaining();
   EXPECT_TRUE(processResult);
   processResult = pool.processRemaining();
   EXPECT_FALSE(processResult);

   pool.shutdown();
}

TEST_F(ThreadPoolTestFixture, test_resetFails_whileTaksIsProcessed)
{
   std::atomic<bool> done = { false };
   utils::ThreadPool<2> pool;
   pool.addTask([&done](){
      while(!done) {}
   });

   pool.processRemaining();
   EXPECT_FALSE(pool.reset());
   done = true;
   pool.shutdown();
}

TEST_F(ThreadPoolTestFixture, test_resetPasses_afterProcessingFinished)
{
   std::atomic<bool> done = { false };
   utils::ThreadPool<2> pool;
   pool.addTask([&done](){
      while(!done) {}
   });

   pool.processRemaining();
   size_t numFailedResets = 0;
   done = true;

   while (!pool.reset())
   {
      ++numFailedResets;
   }
   EXPECT_GE(numFailedResets, 0);
   SUCCEED();
   pool.shutdown();
}

TEST_F(ThreadPoolTestFixture, test_reusePool)
{
   std::atomic<uint32_t> numTask = { 0 };
   utils::ThreadPool<2> pool;
   pool.addTask([&numTask](){
      ++numTask;
   });
   pool.addTask([&numTask](){
      ++numTask;
   });
   pool.addTask([&numTask](){
      ++numTask;
   });

   pool.processRemaining();
   std::future<void> future = pool.getFuture();
   future.get();
   EXPECT_EQ(3, numTask);

   EXPECT_TRUE(pool.reset());
   pool.addTask([&numTask](){
      ++numTask;
   });
   pool.addTask([&numTask](){
      ++numTask;
   });

   pool.processRemaining();
   future = pool.getFuture();
   future.get();
   EXPECT_EQ(5, numTask);

   pool.shutdown();
}

} // namespace test
