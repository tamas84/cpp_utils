//Copyright (c) <2022> <Tamas Gergely>

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this file (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.


#pragma once

#include <atomic>
#include <cassert>
#include <condition_variable>
#include <future>
#include <list>
#include <mutex>
#include <thread>
#include <vector>


namespace utils
{

template<size_t value>
concept ProperThreadNum =
      value > 0;

template <uint32_t numThreads>
requires ProperThreadNum< numThreads >
class ThreadPool
{
   using Func = std::function<void()>;
public:
   ThreadPool()
   {
      m_threads.reserve(numThreads);
      for (uint32_t i = 0; i < numThreads; ++i)
      {
         m_threads.push_back(std::thread([this]() { threadEntry(); }));
      }
   }

   ThreadPool(const ThreadPool&) = delete;
   ThreadPool(ThreadPool&&) = delete;

   bool addTask(Func func)
   {
      if (m_state != State::waitingForTasks)
      {
         return false;
      }

      {
         std::lock_guard<std::mutex> lock(m_mutex);
         m_tasks.push_back(std::move(func));
      }
      ++m_numTasksAdded;
      m_condition.notify_one();

      return true;
   }

   bool processRemaining()
   {
      if (m_state != State::waitingForTasks)
      {
         return false;
      }

      m_state = State::processing;
      checkIfDone();
      return true;
   }

   bool reset()
   {
      if (m_state != State::processingFinished)
      {
         return false;
      }

      m_numTasksAdded = 0;
      m_numTasksProcessed = 0 ;

      assert(m_tasks.empty());

      m_promise = std::promise<void>();
      m_state = State::waitingForTasks;

      return true;
   }

   bool isAlive() const
   {
      return m_state != State::shuttingDown;
   }

   void shutdown()
   {
      m_state = State::shuttingDown;
      m_condition.notify_all();

      for (auto& thread : m_threads)
      {
         thread.join();
      }
   }

   std::future<void> getFuture()
   {
      return m_promise.get_future();
   }

private:
   void threadEntry()
   {
      while (true)
      {
         Func func;
         {
            std::unique_lock<std::mutex> lock(m_mutex);
            m_condition.wait(lock, [this](){
               return m_state == State::shuttingDown || !m_tasks.empty();
            });
            if (m_state == State::shuttingDown)
            {
               break;
            }

            if (!m_tasks.empty())
            {
               func = std::move(m_tasks.front());
               m_tasks.pop_front();
            }
         }

         func();
         ++m_numTasksProcessed;

         checkIfDone();
      }
   }

   void checkIfDone()
   {
      if (m_state == State::processing
          && m_numTasksProcessed == m_numTasksAdded)
      {
         std::lock_guard<std::mutex> lock(m_mutex);
         if (m_state == State::processing)
         {
            m_state = State::processingFinished;
            m_promise.set_value();
         }
      }
   }

   enum class State
   {
      waitingForTasks = 0, // possible to add tasks (task will be processed immediately if there is a free thread)
      processing,          // no more task can be added
      processingFinished,  // all tasks are processed
      shuttingDown         // pool is shutting down
   };

   std::atomic<State> m_state { State::waitingForTasks };
   std::atomic<uint32_t> m_numTasksAdded{ 0 };
   std::atomic<uint32_t> m_numTasksProcessed{ 0 };

   std::vector<std::thread> m_threads;
   std::list<Func> m_tasks;
   std::condition_variable m_condition;
   std::mutex m_mutex;

   std::promise<void> m_promise;
};

} // namespace utils
