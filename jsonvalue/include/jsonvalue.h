#pragma once

#include <cmath>
#include <functional>
#include <limits>
#include <list>
#include <map>
#include <optional>
#include <stdexcept>
#include <string>
#include <string_view>
//#include <unordered_map>
#include <utility> // std::as_const
#include <variant>
#include <vector>

namespace json
{

//template<typename Data>
//class PointerToData
//{
//public:
//   template<typename ...Arg>
//   PointerToData(Arg&&... arg)
//      : data{std::forward<Arg>(arg)...}
//   {}
//   Data* operator -> ()
//   {
//      return &data;
//   }
//private:
//   Data data;
//};

namespace detail
{
// The following is used only for unordered_map
// The key of our unordered_map is std::string. From C++20 heterogeneous lookup is possible for
// unordered_map as well (e.g. pass string_view to the find() function of the map).
// https://www.codeproject.com/Tips/5255442/Cplusplus14-20-Heterogeneous-Lookup-Benchmark
//struct Equal : public std::equal_to<>
//{
//   using is_transparent = void;
//};

//struct StringHash
//{
//   using is_transparent = void;
//   using key_equal = std::equal_to<>;  // Pred to use
//   using hash_type = std::hash<std::string_view>;  // just a helper local type
//   size_t operator()(std::string_view txt) const { return hash_type{}(txt); }
//   size_t operator()(const std::string& txt) const { return hash_type{}(txt); }
//   size_t operator()(const char* txt) const { return hash_type{}(txt); }
//};

} // namespace detail

class JsonValueObjectException : public std::runtime_error
{
public:
   JsonValueObjectException(const std::string& msg)
      : std::runtime_error(msg)
   {}
};

class JsonValue;

class JsonValueObject
{
public:
   JsonValueObject() = default;

   JsonValueObject(const JsonValueObject&) = default;
   JsonValueObject(JsonValueObject&&) noexcept = default;

   JsonValueObject& operator=(const JsonValueObject&) = default;
   JsonValueObject& operator=(JsonValueObject&&) = default;

   size_t getSize() const { return m_value.size(); }

   bool isEmpty() const { return m_value.empty(); }

   void clear() { m_value.clear(); }

   template <typename T>
   bool hasElem(std::string_view name) const;

   template <typename T>
   T getAs(std::string_view name) const;

   template <typename T>
   T getOrAs(std::string_view name, T defaultValue) const;

   template <typename T>
   std::optional<T> getOptAs(std::string_view name) const;

   inline void insert(std::string name, JsonValue value);

   template <typename ...Args>
   void emplace(std::string name, Args... args);

   inline void erase(std::string_view name);

   std::map<std::string, JsonValue, std::less<>>::iterator begin() noexcept { return m_value.begin(); }
   std::map<std::string, JsonValue, std::less<>>::const_iterator begin() const noexcept { return m_value.begin(); }
   std::map<std::string, JsonValue, std::less<>>::const_iterator cbegin() const noexcept { return m_value.begin(); }

   std::map<std::string, JsonValue, std::less<>>::iterator end() noexcept { return m_value.end(); }
   std::map<std::string, JsonValue, std::less<>>::const_iterator end() const noexcept { return m_value.end(); }
   std::map<std::string, JsonValue, std::less<>>::const_iterator cend() const noexcept { return m_value.end(); }

private:
   std::map<std::string, JsonValue, std::less<>> m_value;
};



class JsonValue
{
public:
   using JsonValueType = std::variant<std::nullptr_t, bool, double, std::string, std::vector<JsonValue>, JsonValueObject>;

   JsonValue() = delete;

   JsonValue(const JsonValue&) = default;

   JsonValue(JsonValue&&) noexcept = default;

   template<typename T>
   JsonValue(const T v)
      : m_value(v)
   {
   }

   JsonValue(std::vector<JsonValue> v)
      : m_value(std::move(v))
   {
   }

   template<typename T>
   JsonValue(const std::vector<T>& v)
      : m_value([&v = std::as_const(v)]()
   {
      std::vector<JsonValue> jsonVec;
      jsonVec.reserve(v.size());
      for (const auto& elem: v)
      {
         jsonVec.push_back(std::move(elem));
      }
      return jsonVec;
   }())
   {
   }

   JsonValue(JsonValueObject v)
      : m_value(std::move(v))
   {
   }

public:
   template<typename T>
   bool isA() const
   {
      return isA(static_cast<T*>(nullptr));
   }

   template<typename T>
   std::optional<T> asA() const
   {
      return asA(static_cast<T*>(nullptr));
   }

private:
   template<typename T>
   bool isA(T*) const
   {
      return std::holds_alternative<T>(m_value);
   }

   //   template<> bool isA(int32_t*) const;
   //   template<> bool isA(uint32_t*) const;
   //   template<> bool isA(JsonValue*) const;

   template<typename T>
   bool isA(std::vector<T>*) const
   {
      if (!std::holds_alternative<std::vector<JsonValue>>(m_value))
      {
         return false;
      }

      const auto vector = std::get<std::vector<JsonValue>>(m_value);

      for(const auto& elem : vector)
      {
         if (!elem.isA<T>())
         {
            return false;
         }
      }
      return true;
   }

   template<typename T>
   std::optional<T> asA(T*) const
   {
      if (isA<T>())
      {
         return std::get<T>(m_value);
      }
      return std::optional<T>{};
   }

   inline std::optional<int32_t> asA(int32_t*) const;

   inline std::optional<uint32_t> asA(uint32_t*) const;

   inline std::optional<float> asA(float*) const;

   template<typename T>
   std::optional<std::vector<T>> asA(std::vector<T>*) const
   {
      if (isA<std::vector<T>>())
      {
         const auto& vec = std::get<std::vector<JsonValue>>(m_value);
         std::vector<T> result;
         result.reserve(vec.size());

         for(const auto& elem : vec)
         {
            if (!elem.isA<T>())
            {
               return std::optional<std::vector<T>>{};
            }
            else
            {
               result.push_back(elem.asA<T>().value());
            }
         }

         return result;
      }
      return std::optional<std::vector<T>>{};
   }

   inline std::optional<std::vector<JsonValue>> asA(std::vector<JsonValue>*) const;

private:
   inline bool isDouble() const;

   JsonValueType m_value;
};


#include "jsonvalue_impl.h"

} // namespace util
