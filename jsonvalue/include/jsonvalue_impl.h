// JsonValue //

template<> inline bool JsonValue::isA<int32_t>(int32_t*) const
{
   if (!isDouble())
   {
      return false;
   }

   double storedValue = std::get<double>(m_value);
   if (storedValue > std::numeric_limits<int32_t>::max()
       || storedValue < std::numeric_limits<int32_t>::lowest())
   {
      return false;
   }

   double truncValue = std::trunc(storedValue);
   if (storedValue != truncValue)
   {
      return false;
   }

   return true;
}

template<> inline bool JsonValue::isA<uint32_t>(uint32_t*) const
{
   if (!isDouble())
   {
      return false;
   }

   double storedValue = std::get<double>(m_value);
   if (storedValue > std::numeric_limits<uint32_t>::max()
       || storedValue < std::numeric_limits<uint32_t>::lowest())
   {
      return false;
   }

   double truncValue = std::trunc(storedValue);
   if (storedValue != truncValue)
   {
      return false;
   }

   return true;
}

template<> inline bool JsonValue::isA<float>(float*) const
{
   if (!isDouble())
   {
      return false;
   }

   double storedValue = std::get<double>(m_value);
   if (storedValue > std::numeric_limits<float>::max()
       || storedValue < std::numeric_limits<float>::lowest())
   {
      return false;
   }

   return true;
}

template<> inline bool JsonValue::isA<JsonValue>(JsonValue*) const
{
   return true;
}

std::optional<int32_t> JsonValue::asA(int32_t*) const
{
   if (isA<int32_t>())
   {
      return static_cast<int32_t>(std::get<double>(m_value));
   }
   return std::optional<int32_t>{};
}

std::optional<uint32_t> JsonValue::asA(uint32_t*) const
{
   if (isA<uint32_t>())
   {
      return static_cast<uint32_t>(std::get<double>(m_value));
   }
   return std::optional<uint32_t>{};
}

std::optional<float> JsonValue::asA(float*) const
{
   if (isA<float>())
   {
      return static_cast<float>(std::get<double>(m_value));
   }
   return std::optional<float>{};
}

std::optional<std::vector<JsonValue>> JsonValue::asA(std::vector<JsonValue>*) const
{
   return std::get<std::vector<JsonValue>>(m_value);
}

bool JsonValue::isDouble() const
{
   return std::holds_alternative<double>(m_value);
}


// JsonValueObject //

// used only for the unordered_map / list implementation, wehere the map only stores
// the list::iterators instead of JsonValue.
//inline JsonValueObject::JsonValueObject(const JsonValueObject& o)
//{
//   for (const auto& [name, listIt] : o.m_index)
//   {
//      const JsonValue& jv = *listIt;
//      const auto it = m_value.insert(m_value.end(), jv);
//      m_index.emplace(name, it);
//   }
//}

template <typename T>
bool JsonValueObject::hasElem(std::string_view name) const
{
   const auto it = m_value.find(name);
   if (it == m_value.end())
   {
      return false;
   }

   return it->second.isA<T>();
}

template <typename T>
T JsonValueObject::getAs(std::string_view name) const
{
   if (hasElem<T>(name))
   {
      return m_value.find(name)->second.asA<T>().value();
   }

   std::string msg{ "Element with key: '" };
   msg.append(name);
   msg.append("' not found.");
   throw JsonValueObjectException(msg);
}

template <typename T>
T JsonValueObject::getOrAs(std::string_view name, T defaultValue) const
{
   if (hasElem<T>(name))
   {
      return m_value.find(name)->second.asA<T>().value();
   }

   return std::move(defaultValue);
}

template <typename T>
std::optional<T> JsonValueObject::getOptAs(std::string_view name) const
{
   if (hasElem<T>(name))
   {
      return m_value.find(name)->second.asA<T>().value();
   }

   return std::nullopt;
}

inline void JsonValueObject::insert(std::string name, JsonValue value)
{
   if (m_value.find(name) != m_value.end())
   {
      return;
   }

   //const auto it = m_value.insert(m_value.end(), std::move(value));
   m_value.emplace(std::move(name), value);
}

template <typename ...Args>
inline void JsonValueObject::emplace(std::string name, Args... args)
{
   if (m_value.find(name) != m_value.end())
   {
      return;
   }

   const auto it = m_value.emplace(m_value.end(), std::forward(args)...);
   m_value.emplace(std::move(name), it);
}

inline void JsonValueObject::erase(std::string_view name)
{
   const auto it = m_value.find(name);
   if (it == m_value.end())
   {
      return;
   }

   m_value.erase(it);
}


// JsonValueObject iterator in case where unordered_map and list is used
//struct Iterator
//{
//   using iterator_category = std::bidirectional_iterator_tag;
//   using difference_type   = std::ptrdiff_t; //std::reference_wrapper<const JsonValue>
//   using value_type        = std::pair<std::string, std::reference_wrapper<const JsonValue>>;
//   using pointer           = std::pair<std::string, std::reference_wrapper<const JsonValue>>*;
//   using reference         = std::pair<std::string, std::reference_wrapper<const JsonValue>>&;

//   Iterator(JsonValueObject* container, bool begin)
//      : m_container(container)
//   {
//      m_mapIterator = begin ? m_container->m_index.begin()
//                            : m_container->m_index.end();
//   }

//   //      std::pair<std::string, std::reference_wrapper<const JsonValue>>
//   //      std::reference_wrapper<const JsonValue>
//   value_type
//   operator*()
//   {
//      //         return std::ref(*m_mapIterator->second);
//      return std::make_pair(m_mapIterator->first, std::ref(*m_mapIterator->second));
//   }

//   //      std::unordered_map<std::string, std::list<JsonValue>::const_iterator, detail::StringHash, detail::Equal>::const_iterator&
//   PointerToData<value_type>
//   operator->()
//   {
//      return PointerToData<value_type>(std::make_pair(m_mapIterator->first, std::ref(*m_mapIterator->second)));
//   }

//   //      std::unordered_map<std::string, std::list<JsonValue>::const_iterator, detail::StringHash, detail::Equal>::const_iterator*
//   //      operator->()
//   //      {
//   //         return &m_mapIterator;
//   //      }

//   Iterator& operator++()
//   {
//      m_mapIterator++;
//      return *this;
//   }

//   Iterator operator++(int)
//   {
//      Iterator tmp = *this;
//      ++(*this);
//      return tmp;
//   }

//   friend bool operator== (const Iterator& a, const Iterator& b)
//   {
//      return a.m_mapIterator == b.m_mapIterator;
//   };

//   friend bool operator!= (const Iterator& a, const Iterator& b)
//   {
//      return a.m_mapIterator != b.m_mapIterator;
//   };

//private:
//   JsonValueObject* m_container;
//   std::unordered_map<std::string, std::list<JsonValue>::const_iterator, detail::StringHash, detail::Equal>::iterator m_mapIterator;
//};

//Iterator begin()
//{
//   return Iterator(this, true);
//}

//Iterator end()
//{
//   return Iterator(this, false);
//}
