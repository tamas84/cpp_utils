#ifdef UTILS_GLM_SUPPORT

#pragma once

#include "jsonvalue.h"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <string>
#include <string_view>
#include <vector>

namespace json::json_helper
{

class GlmJsonHelper : public JsonValueObjectException
{
public:
   GlmJsonHelper(const std::string& msg)
      : JsonValueObjectException(msg)
   {}
};

//==== glm::vec helper functions ====
template<glm::length_t N>
void insert(JsonValueObject& object, const std::string& name, const glm::vec<N, float, glm::defaultp>& vec)
{
   std::vector<double> v;
   v.reserve(N);

   for (uint32_t i = 0; i < N; ++i)
   {
      v.push_back(vec[i]);
   }

   object.insert(name, v);
}

template<glm::length_t N>
bool hasVec(const JsonValueObject& object, std::string_view name)
{
   if (!object.hasElem<std::vector<double>>(name))
   {
      return false;
   }

   const auto& vecDouble = object.getAs<std::vector<double>>(name);
   if (vecDouble.size() != N)
   {
      return false;
   }

   return true;
}

template<glm::length_t N>
glm::vec<N, float, glm::defaultp> getVec(const JsonValueObject& object, const std::string& name)
{
   if (!object.hasElem<std::vector<double>>(name))
   {
      std::string msg{ "Element with key: '" };
      msg.append(name);
      msg.append("' not found.");
      throw GlmJsonHelper(msg);
   }

   const auto vecDouble = object.getAs<std::vector<double>>(name);
   if (vecDouble.size() != N)
   {
      std::string msg{ "Element with key: '" };
      msg.append(name);
      msg.append("' found, but its size is incorrect. It is: '");
      msg.append(std::to_string(vecDouble.size()));
      msg.append("' should be: '");
      msg.append(std::to_string(N));
      msg.append("'.");
      throw GlmJsonHelper(msg);
   }

   glm::vec<N, float, glm::defaultp> glmVec;
   for (glm::length_t i = 0; i < N; ++i)
   {
      glmVec[i] = vecDouble[i];
   }

   return glmVec;
}

template<glm::length_t N>
glm::vec<N, float, glm::defaultp> getVecOr(const JsonValueObject& object, const std::string& name, glm::vec<N, float, glm::defaultp> defaultValue)
{
   if (!object.hasElem<std::vector<double>>(name))
   {
      return defaultValue;
   }

   const auto& vecDouble = object.getAs<std::vector<double>>(name);
   if (vecDouble.size() != N)
   {
      return defaultValue;
   }

   glm::vec<N, float, glm::defaultp> glmVec;
   for (glm::length_t i = 0; i < N; ++i)
   {
      glmVec[i] = vecDouble[i];
   }

   return glmVec;
}

template<glm::length_t N>
glm::vec<N, float, glm::defaultp> getVecOrDefault(const JsonValueObject& object, const std::string& name)
{
   return getVecOr(object, name, glm::vec<N, float, glm::defaultp>{});
}


//==== glm::mat helper functions ====
template<glm::length_t N>
void insert(JsonValueObject& object, const std::string& name, const glm::mat<N, N, float, glm::defaultp>& mat)
{
   // glm::mat is column-major, save its values in row-major form
   std::vector<double> v;
   v.reserve(N * N);

   //      const auto matTrans = glm::transpose(mat);
   //      const float* m = static_cast<const float*>(glm::value_ptr(matTrans));
   //      for (uint32_t i = 0; i < N * N; ++i)
   //      {
   //         v.push_back(m[i]);
   //      }
   //      or:

   for (glm::length_t r = 0; r < N; ++r)
   {
      for (glm::length_t c = 0; c < N; ++c)
      {
         v.push_back(mat[c][r]);
      }
   }

   object.insert(name, std::move(v));
}

template<glm::length_t N>
bool hasMat(const JsonValueObject& object, std::string_view name)
{
   if (!object.hasElem<std::vector<double>>(name))
   {
      return false;
   }

   const auto& vecDouble = object.getAs<std::vector<double>>(name);
   constexpr glm::length_t SIZE = N * N;
   if (vecDouble.size() != SIZE)
   {
      return false;
   }

   return true;
}

template<glm::length_t N>
glm::mat<N, N, float, glm::defaultp> getMat(const JsonValueObject& object, const std::string& name)
{
   if (!object.hasElem<std::vector<double>>(name))
   {
      std::string msg{ "Element with key: '" };
      msg.append(name);
      msg.append("' not found.");
      throw GlmJsonHelper(msg);
   }

   const auto vecDouble = object.getAs<std::vector<double>>(name);
   constexpr glm::length_t SIZE = N * N;
   if (vecDouble.size() != SIZE)
   {
      std::string msg{ "Element with key: '" };
      msg.append(name);
      msg.append("' found, but its size is incorrect. It is: '");
      msg.append(std::to_string(vecDouble.size()));
      msg.append("' should be: '");
      msg.append(std::to_string(SIZE));
      msg.append("'.");
      throw GlmJsonHelper(msg);
   }

   glm::mat<N, N, float, glm::defaultp> glmMat;

   glm::length_t step = 0;
   for (glm::length_t r = 0; r < N; ++r)
   {
      for (glm::length_t c = 0; c < N; ++c)
      {
         glmMat[c][r] = vecDouble[step];
         ++step;
      }
   }

   return glmMat;
}

template<glm::length_t N>
glm::mat<N, N, float, glm::defaultp> getMatOr(const JsonValueObject& object, const std::string& name, glm::mat<N, N, float, glm::defaultp> defaultValue)
{
   if (!object.hasElem<std::vector<double>>(name))
   {
      return defaultValue;
   }

   const auto& vecDouble = object.getAs<std::vector<double>>(name);
   constexpr glm::length_t SIZE = N * N;
   if (vecDouble.size() != SIZE)
   {
      return defaultValue;
   }

   glm::mat<N, N, float, glm::defaultp> glmMat;

   glm::length_t step = 0;
   for (glm::length_t r = 0; r < N; ++r)
   {
      for (glm::length_t c = 0; c < N; ++c)
      {
         glmMat[c][r] = vecDouble[step];
         ++step;
      }
   }

   return glmMat;
}

template<glm::length_t N>
glm::mat<N, N, float, glm::defaultp> getMatOrDefault(const JsonValueObject& object, const std::string& name)
{
   return getMatOr(object, name, glm::mat<N, N, float, glm::defaultp>{});
}

} // namespace json::json_helper

#endif // UTILS_GLM_SUPPORT
