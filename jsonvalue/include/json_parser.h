#pragma once

#include "jsonvalue.h"

#include <string>
namespace json
{

JsonValueObject serializeFromFile(const std::string& fileName);

} // namespace json

