#include <gtest/gtest.h>

#include "jsonvalue.h"


using namespace json;

namespace test
{


TEST(JsonValue, noexcept_move_ctor)
{
   static_assert(std::is_nothrow_move_constructible<JsonValue>::value, "JsonValue should be noexcept MoveConstructible");
}

TEST(JsonValueObject, noexcept_move_ctor)
{
   static_assert(std::is_nothrow_move_constructible<JsonValueObject>::value, "JsonValueObject should be noexcept MoveConstructible");
}

TEST(JsonValue, jsonvalue_nullptr)
{
   JsonValue value{nullptr};
   EXPECT_TRUE(value.isA<std::nullptr_t>());
   EXPECT_FALSE(value.isA<bool>());
   EXPECT_FALSE(value.isA<double>());
   EXPECT_FALSE(value.isA<std::string>());

   EXPECT_TRUE(value.asA<std::nullptr_t>().has_value());
   EXPECT_FALSE(value.asA<bool>().has_value());
   EXPECT_FALSE(value.asA<double>().has_value());
   EXPECT_FALSE(value.asA<std::string>().has_value());

   EXPECT_EQ(nullptr, value.asA<std::nullptr_t>().value());
}

TEST(JsonValue, jsonvalue_bool)
{
   JsonValue value{true};
   EXPECT_FALSE(value.isA<std::nullptr_t>());
   EXPECT_TRUE(value.isA<bool>());
   EXPECT_FALSE(value.isA<double>());
   EXPECT_FALSE(value.isA<std::string>());
   EXPECT_TRUE(value.asA<bool>().has_value());
   EXPECT_FALSE(value.asA<double>().has_value());
   EXPECT_FALSE(value.asA<std::string>().has_value());
   EXPECT_TRUE(value.asA<bool>().value());

   JsonValue value_false{false};
   EXPECT_FALSE(value_false.asA<bool>().value());
}

TEST(JsonValue, jsonvalue_double)
{
   JsonValue value{2.1};
   EXPECT_FALSE(value.isA<std::nullptr_t>());
   EXPECT_FALSE(value.isA<bool>());
   EXPECT_TRUE(value.isA<double>());
   EXPECT_FALSE(value.isA<std::string>());
   EXPECT_FALSE(value.asA<bool>().has_value());
   EXPECT_TRUE(value.asA<double>().has_value());
   EXPECT_FALSE(value.asA<std::string>().has_value());
   EXPECT_DOUBLE_EQ(2.1, value.asA<double>().value());
}

TEST(JsonValue, jsonvalue_double_as_int)
{
   JsonValue value1{2.0};
   EXPECT_FALSE(value1.isA<std::nullptr_t>());
   EXPECT_FALSE(value1.isA<bool>());
   EXPECT_TRUE(value1.isA<int>());
   EXPECT_TRUE(value1.isA<unsigned int>());
   EXPECT_TRUE(value1.isA<int32_t>());
   EXPECT_TRUE(value1.isA<uint32_t>());
   EXPECT_FALSE(value1.isA<std::string>());
   EXPECT_FALSE(value1.asA<bool>().has_value());
   EXPECT_TRUE(value1.asA<int>().has_value());
   EXPECT_TRUE(value1.asA<unsigned int>().has_value());
   EXPECT_FALSE(value1.asA<std::string>().has_value());
   EXPECT_EQ(2, value1.asA<int>().value());
   EXPECT_EQ(2, value1.asA<unsigned int>().value());
   EXPECT_EQ(2, value1.asA<int32_t>().value());
   EXPECT_EQ(2, value1.asA<uint32_t>().value());

   JsonValue value2{2.1};
   EXPECT_FALSE(value2.isA<std::nullptr_t>());
   EXPECT_FALSE(value2.isA<bool>());
   EXPECT_FALSE(value2.isA<int>());
   EXPECT_FALSE(value2.isA<unsigned int>());
   EXPECT_FALSE(value2.isA<int32_t>());
   EXPECT_FALSE(value2.isA<uint32_t>());
   EXPECT_FALSE(value2.isA<std::string>());
   EXPECT_FALSE(value2.asA<bool>().has_value());
   EXPECT_FALSE(value2.asA<int>().has_value());
   EXPECT_FALSE(value2.asA<unsigned int>().has_value());
   EXPECT_FALSE(value2.asA<std::string>().has_value());
}

TEST(JsonValue, jsonvalue_string)
{
   JsonValue value{std::string("TestString")};
   EXPECT_FALSE(value.isA<std::nullptr_t>());
   EXPECT_FALSE(value.isA<bool>());
   EXPECT_FALSE(value.isA<double>());
   EXPECT_TRUE(value.isA<std::string>());
   EXPECT_FALSE(value.asA<bool>().has_value());
   EXPECT_FALSE(value.asA<double>().has_value());
   EXPECT_TRUE(value.asA<std::string>().has_value());
   EXPECT_EQ("TestString", value.asA<std::string>().value());
}

TEST(JsonValue, jsonvalue_vector_jsonvalue_with_bools)
{
   std::vector<JsonValue> vec{true, false, true};
   JsonValue value{vec};

   EXPECT_FALSE(value.isA<bool>());
   EXPECT_FALSE(value.isA<double>());
   EXPECT_FALSE(value.isA<std::string>());
   EXPECT_TRUE(value.isA<std::vector<JsonValue>>());
   EXPECT_FALSE(value.asA<bool>().has_value());
   EXPECT_FALSE(value.asA<double>().has_value());
   EXPECT_FALSE(value.asA<std::string>().has_value());
   EXPECT_TRUE(value.asA<std::vector<JsonValue>>().has_value());

   std::vector<JsonValue> otherValue = value.asA<std::vector<JsonValue>>().value();
   EXPECT_EQ(3, otherValue.size());
   EXPECT_EQ(true, otherValue[0].asA<bool>().value());
   EXPECT_EQ(false, otherValue[1].asA<bool>().value());
   EXPECT_EQ(true, otherValue[2].asA<bool>().value());
}

TEST(JsonValue, jsonvalue_vector_jsonvalue_with_doubles)
{
   std::vector<JsonValue> vec{1.9f, 2.2f, 3.7f};
   JsonValue value{vec};

   EXPECT_FALSE(value.isA<bool>());
   EXPECT_FALSE(value.isA<double>());
   EXPECT_FALSE(value.isA<std::string>());
   EXPECT_TRUE(value.isA<std::vector<JsonValue>>());
   EXPECT_FALSE(value.asA<bool>().has_value());
   EXPECT_FALSE(value.asA<double>().has_value());
   EXPECT_FALSE(value.asA<std::string>().has_value());
   EXPECT_TRUE(value.asA<std::vector<JsonValue>>().has_value());

   std::vector<JsonValue> otherValue = value.asA<std::vector<JsonValue>>().value();
   EXPECT_EQ(3, otherValue.size());
   EXPECT_DOUBLE_EQ(1.9f, otherValue[0].asA<double>().value());
   EXPECT_DOUBLE_EQ(2.2f, otherValue[1].asA<double>().value());
   EXPECT_DOUBLE_EQ(3.7f, otherValue[2].asA<double>().value());
}

TEST(JsonValue, jsonvalue_vector_with_doubles)
{
   std::vector<double> doubleVec{1.9f, 2.2f, 3.7f};
   JsonValue value{ doubleVec };

   EXPECT_FALSE(value.isA<bool>());
   EXPECT_FALSE(value.isA<double>());
   EXPECT_FALSE(value.isA<std::string>());
   EXPECT_TRUE(value.isA<std::vector<JsonValue>>());
   EXPECT_FALSE(value.asA<bool>().has_value());
   EXPECT_FALSE(value.asA<double>().has_value());
   EXPECT_FALSE(value.asA<std::string>().has_value());
   EXPECT_TRUE(value.asA<std::vector<JsonValue>>().has_value());

   std::vector<double> otherValue = value.asA<std::vector<double>>().value();
   EXPECT_EQ(3, otherValue.size());
   EXPECT_DOUBLE_EQ(1.9f, otherValue[0]);
   EXPECT_DOUBLE_EQ(2.2f, otherValue[1]);
   EXPECT_DOUBLE_EQ(3.7f, otherValue[2]);
}

TEST(JsonValue, jsonvalue_vector_with_strings)
{
   std::vector<std::string> stringVec{"11", "22", "33"};
   JsonValue value{ stringVec };

   EXPECT_FALSE(value.isA<bool>());
   EXPECT_FALSE(value.isA<double>());
   EXPECT_FALSE(value.isA<std::string>());
   EXPECT_TRUE(value.isA<std::vector<JsonValue>>());
   EXPECT_FALSE(value.asA<bool>().has_value());
   EXPECT_FALSE(value.asA<double>().has_value());
   EXPECT_FALSE(value.asA<std::string>().has_value());
   EXPECT_TRUE(value.asA<std::vector<JsonValue>>().has_value());

   std::vector<std::string> otherValue = value.asA<std::vector<std::string>>().value();
   EXPECT_EQ(3, otherValue.size());
   EXPECT_EQ("11", otherValue[0]);
   EXPECT_EQ("22", otherValue[1]);
   EXPECT_EQ("33", otherValue[2]);
}

TEST(JsonValueObject, jvo_tests) // TODO
{
   JsonValueObject jvo;
   jvo.insert("string", "stringValue");
   jvo.insert("double", 5.0);

   EXPECT_EQ("stringValue", jvo.getAs<std::string>("string"));
   EXPECT_EQ(5.0, jvo.getAs<double>("double"));

   JsonValue jv = jvo;
   JsonValueObject jvo2 = jv.asA<JsonValueObject>().value();
   EXPECT_EQ(2, jvo2.getSize());
   EXPECT_EQ("stringValue", jvo2.getAs<std::string>("string"));
   EXPECT_EQ(5.0, jvo2.getAs<double>("double"));

   std::vector<JsonValue> vec;
   vec.push_back(jvo2);
   EXPECT_EQ(1, vec.size());
   JsonValueObject jvo3 = vec[0].asA<JsonValueObject>().value();
   EXPECT_EQ(2, jvo3.getSize());
   EXPECT_EQ("stringValue", jvo3.getAs<std::string>("string"));
   EXPECT_EQ(5.0, jvo3.getAs<double>("double"));
}

} // namespace test



