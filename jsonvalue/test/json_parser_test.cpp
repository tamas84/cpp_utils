#include <gtest/gtest.h>

#include "json_parser.h"


using namespace json;

namespace test
{

class JsonParserFixture : public ::testing::Test
{
public:
   void SetUp() override
   {
   }
protected:
   const JsonValueObject testObject = json::serializeFromFile("test.json");
};

TEST_F(JsonParserFixture, test_size)
{
   EXPECT_EQ(9, testObject.getSize());
}

TEST_F(JsonParserFixture, test_simple_types)
{
   EXPECT_TRUE(testObject.hasElem<bool>("boolean"));
   EXPECT_EQ(testObject.getAs<bool>("boolean"), true);
   EXPECT_TRUE(testObject.hasElem<std::string>("string"));
   EXPECT_EQ(testObject.getAs<std::string>("string"), std::string("hi"));
   EXPECT_TRUE(testObject.hasElem<std::nullptr_t>("null"));
   EXPECT_TRUE(testObject.hasElem<double>("number"));
   EXPECT_DOUBLE_EQ(testObject.getAs<double>("number"), 123.0);
}

TEST_F(JsonParserFixture, test_object)
{
   EXPECT_TRUE(testObject.hasElem<JsonValueObject>("object"));
   const auto obj = testObject.getAs<JsonValueObject>("object");
   EXPECT_EQ(obj.getSize(), 5);
}

TEST_F(JsonParserFixture, test_int_array)
{
   EXPECT_TRUE(testObject.hasElem<JsonValue>("int_array"));
   const auto int_array1 = testObject.getAs<std::vector<JsonValue>>("int_array");
   EXPECT_DOUBLE_EQ(1.0, int_array1[0].asA<double>().value());
   const auto vector1 = testObject.getAs<std::vector<double>>("int_array");
   EXPECT_EQ(vector1.size(), 3);
   EXPECT_DOUBLE_EQ(1.0, vector1[0]);
   EXPECT_DOUBLE_EQ(2.0, vector1[1]);
   EXPECT_DOUBLE_EQ(3.0, vector1[2]);
}

TEST_F(JsonParserFixture, test_str_array)
{
   const auto vector = testObject.getAs<std::vector<std::string>>("str_array");
   EXPECT_EQ("one", vector[0]);
   EXPECT_EQ("two", vector[1]);
   EXPECT_EQ("three", vector[2]);
}

TEST_F(JsonParserFixture, test_obj_array_as_JsonValueObject)
{
   const auto vector = testObject.getAs<std::vector<JsonValueObject>>("obj_array");
   EXPECT_EQ(2, vector.size());

   EXPECT_EQ(3, vector[0].getSize());
   EXPECT_TRUE(vector[0].hasElem<std::string>("aa"));
   EXPECT_TRUE(vector[0].hasElem<std::string>("bb"));
   EXPECT_TRUE(vector[0].hasElem<double>("int"));
   EXPECT_EQ("AA", vector[0].getAs<std::string>("aa"));
   EXPECT_EQ("BB", vector[0].getAs<std::string>("bb"));
   EXPECT_EQ(5, vector[0].getAs<int>("int"));

   EXPECT_EQ(3, vector[1].getSize());
   EXPECT_TRUE(vector[1].hasElem<std::string>("aa"));
   EXPECT_TRUE(vector[1].hasElem<std::string>("bb"));
   EXPECT_TRUE(vector[1].hasElem<double>("int"));
   EXPECT_EQ("AAA", vector[1].getAs<std::string>("aa"));
   EXPECT_EQ("BBB", vector[1].getAs<std::string>("bb"));
   EXPECT_EQ(6, vector[1].getAs<int>("int"));
}

TEST_F(JsonParserFixture, test_obj_array_as_JsonValue)
{
   const auto vector = testObject.getAs<std::vector<JsonValue>>("obj_array");
   EXPECT_EQ(2, vector.size());

   EXPECT_TRUE(vector[0].isA<JsonValue>());
   EXPECT_TRUE(vector[0].isA<JsonValueObject>());
   const auto jvo = vector[0].asA<JsonValueObject>().value();
   EXPECT_EQ(3, jvo.getSize());
}

TEST_F(JsonParserFixture, test_array_array)
{
   const auto vector = testObject.getAs<std::vector<JsonValue>>("array_array");
   EXPECT_EQ(3, vector.size());
   int value = 1;
   for (size_t i = 0; i < vector.size(); ++i)
   {
      const auto innerVec = vector[i].asA<std::vector<double>>();
      EXPECT_TRUE(innerVec.has_value());
      EXPECT_EQ(4, innerVec.value().size());
      for (size_t j = 0; j < innerVec.value().size(); ++j)
      {
          EXPECT_EQ(value++, innerVec.value()[j]);
      }
   }
}

} // namespace test
