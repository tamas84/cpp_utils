#ifdef UTILS_GLM_SUPPORT

#include <gtest/gtest.h>

#include "jsonvalue_glm_helper.h"
#include "jsonvalue.h"


using namespace json;

namespace test
{

class JsonValueGlmHelperFixture : public ::testing::Test
{
public:
   void SetUp() override
   {
   }

protected:
   std::vector<float> rawVec3 = {1.f, 2.f, 3.f};
   glm::vec3 glmVec3{1.f, 2.f, 3.f};
   std::vector<float> rawMat3{ 1.f, 4.f, 7.f,   // 1st row
                               2.f, 5.f, 8.f,   // 2nd row
                               3.f, 6.f, 9.f }; // 3rd row
   glm::mat3 glmMat3{ 1.f, 4.f, 7.f,   // 1st column
                      2.f, 5.f, 8.f,   // 2nd column
                      3.f, 6.f, 9.f }; // 3rd column
   JsonValueObject obj;
};

TEST_F(JsonValueGlmHelperFixture, test_vector_getAs)
{
   obj.insert("rawVec", rawVec3);
   json_helper::insert(obj, "glmVec", glmVec3);

   {
      const auto vec = obj.getAs<std::vector<double>>("rawVec");
      EXPECT_EQ(3, vec.size());
      EXPECT_FLOAT_EQ(1.f, vec[0]);
      EXPECT_FLOAT_EQ(2.f, vec[1]);
      EXPECT_FLOAT_EQ(3.f, vec[2]);
   }

   {
      const auto vec = json_helper::getVec<3>(obj, "rawVec");
      EXPECT_EQ(3, vec.length());
      EXPECT_FLOAT_EQ(1.f, vec[0]);
      EXPECT_FLOAT_EQ(2.f, vec[1]);
      EXPECT_FLOAT_EQ(3.f, vec[2]);
   }

   {
      const auto vec = obj.getAs<std::vector<double>>("glmVec");
      EXPECT_EQ(3, vec.size());
      EXPECT_FLOAT_EQ(1.f, vec[0]);
      EXPECT_FLOAT_EQ(2.f, vec[1]);
      EXPECT_FLOAT_EQ(3.f, vec[2]);
   }

   {
      const auto vec = json_helper::getVec<3>(obj, "glmVec");
      EXPECT_EQ(3, vec.length());
      EXPECT_FLOAT_EQ(1.f, vec[0]);
      EXPECT_FLOAT_EQ(2.f, vec[1]);
      EXPECT_FLOAT_EQ(3.f, vec[2]);
   }
}

TEST_F(JsonValueGlmHelperFixture, test_vector_getVecOr)
{
   const auto vec = json_helper::getVecOr<3>(obj, "notExists", glm::vec3{1.0f, 2.0f, 3.0f});
   EXPECT_EQ(3, vec.length());
   EXPECT_FLOAT_EQ(1.f, vec[0]);
   EXPECT_FLOAT_EQ(2.f, vec[1]);
   EXPECT_FLOAT_EQ(3.f, vec[2]);
}

TEST_F(JsonValueGlmHelperFixture, test_vector_getVecOrDefault)
{
   const auto vec = json_helper::getVecOrDefault<3>(obj, "notExists");
   EXPECT_EQ(3, vec.length());
   EXPECT_FLOAT_EQ(0.f, vec[0]);
   EXPECT_FLOAT_EQ(0.f, vec[1]);
   EXPECT_FLOAT_EQ(0.f, vec[2]);
}

TEST_F(JsonValueGlmHelperFixture, test_matrix)
{
   obj.insert("rawMat3", rawMat3);
   json_helper::insert(obj, "glmMat", glmMat3);

   {
      const auto& vec = obj.getAs<std::vector<double>>("rawMat3");
      EXPECT_EQ(9, vec.size());

      EXPECT_FLOAT_EQ(1.f, vec[0]);
      EXPECT_FLOAT_EQ(4.f, vec[1]);
      EXPECT_FLOAT_EQ(7.f, vec[2]);
      EXPECT_FLOAT_EQ(2.f, vec[3]);
      EXPECT_FLOAT_EQ(5.f, vec[4]);
      EXPECT_FLOAT_EQ(8.f, vec[5]);
      EXPECT_FLOAT_EQ(3.f, vec[6]);
      EXPECT_FLOAT_EQ(6.f, vec[7]);
      EXPECT_FLOAT_EQ(9.f, vec[8]);


      const auto mat = json_helper::getMat<3>(obj, "rawMat3");
      EXPECT_EQ(3, mat.length());
      EXPECT_FLOAT_EQ(1.f, mat[0][0]);
      EXPECT_FLOAT_EQ(2.f, mat[0][1]);
      EXPECT_FLOAT_EQ(3.f, mat[0][2]);
      EXPECT_FLOAT_EQ(4.f, mat[1][0]);
      EXPECT_FLOAT_EQ(5.f, mat[1][1]);
      EXPECT_FLOAT_EQ(6.f, mat[1][2]);
      EXPECT_FLOAT_EQ(7.f, mat[2][0]);
      EXPECT_FLOAT_EQ(8.f, mat[2][1]);
      EXPECT_FLOAT_EQ(9.f, mat[2][2]);
   }

   {
      const auto& mat1 = obj.getAs<std::vector<double>>("glmMat");
      EXPECT_EQ(9, mat1.size());
      for (int i = 0; i < 9; ++i)
      {
         EXPECT_FLOAT_EQ(i + 1.0, mat1[i]);
      }

      const auto mat2 = json_helper::getMat<3>(obj, "glmMat");
      EXPECT_EQ(3, mat2.length());
      EXPECT_EQ(glmMat3, mat2);
      EXPECT_FLOAT_EQ(1.f, mat2[0][0]);
      EXPECT_FLOAT_EQ(4.f, mat2[0][1]);
      EXPECT_FLOAT_EQ(7.f, mat2[0][2]);
      EXPECT_FLOAT_EQ(2.f, mat2[1][0]);
      EXPECT_FLOAT_EQ(5.f, mat2[1][1]);
      EXPECT_FLOAT_EQ(8.f, mat2[1][2]);
      EXPECT_FLOAT_EQ(3.f, mat2[2][0]);
      EXPECT_FLOAT_EQ(6.f, mat2[2][1]);
      EXPECT_FLOAT_EQ(9.f, mat2[2][2]);
   }
}

TEST_F(JsonValueGlmHelperFixture, test_matrix_getMatOr)
{
   const auto mat = json_helper::getMatOr<3>(obj, "notExists", glm::mat4(1.0f));
   for (int i = 0; i < 3; ++i)
   {
      for (int j = 0; j < 3; ++j)
      {
         if (i == j)
         {
            EXPECT_FLOAT_EQ(1.f, mat[i][j]);
         }
         else
         {
            EXPECT_FLOAT_EQ(0.f, mat[i][j]);
         }
      }
   }
}

TEST_F(JsonValueGlmHelperFixture, test_matrix_getMatOrDefault)
{
   const auto mat = json_helper::getMatOrDefault<3>(obj, "notExists");
   for (int i = 0; i < 3; ++i)
   {
      for (int j = 0; j < 3; ++j)
      {
         EXPECT_FLOAT_EQ(0.f, mat[i][j]);
      }
   }
}

} // namespace test

#endif // UTILS_GLM_SUPPORT
