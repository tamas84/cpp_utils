#include "json_parser.h"

#include <fstream>
#include <cassert>
#include <json/json.h>

namespace
{
using namespace json;

std::vector<JsonValue> parseArray(const Json::Value& json);
JsonValueObject parseObject(const Json::Value& json);

bool isSingleElement(const Json::Value& node)
{
   if (node.isNumeric() || node.isString() || node.isBool() || node.isNull())
   {
      return true;
   }
   return false;
}

bool isArray(const Json::Value& node)
{
   return node.isArray();
}

bool isObject(const Json::Value& node)
{
   return node.isObject();
}

JsonValue parseSingleElement(Json::Value::const_iterator elemIt)
{
   if(elemIt->isNull())
   {
      return JsonValue(nullptr);
   }
   else if (elemIt->isNumeric())
   {
      return elemIt->asDouble();
   }
   else if (elemIt->isString())
   {
      return elemIt->asString();
   }
   else if (elemIt->isBool())
   {
      return elemIt->asBool();
   }

   assert(false && "parseSingleElement unhandled case" );
   return JsonValue(nullptr);
}

std::vector<JsonValue> parseArray(const Json::Value& json)
{
   std::vector<JsonValue> result;
   for(auto it = json.begin(); it != json.end(); ++it)
   {
      if (isSingleElement(*it))
      {
         result.push_back(parseSingleElement(it));
      }
      else if (isArray(*it))
      {
         result.push_back(parseArray(*it));
      }
      else if (isObject(*it))
      {
         result.push_back(parseObject(*it));
      }
      else
      {
         assert(false && "parseArray unhandled case" );
      }
   }

   return result;
}

JsonValueObject parseObject(const Json::Value& json)
{
   JsonValueObject result;
   for(auto it = json.begin(); it != json.end(); ++it)
   {
      if (isSingleElement(*it))
      {
         result.insert(it.name(), parseSingleElement(it));
      }
      else if (isArray(*it))
      {
         result.insert(it.name(), parseArray(*it));
      }
      else if (isObject(*it))
      {
         result.insert(it.name(), parseObject(*it));
      }
      else
      {
         assert(false && "parseObject unhandled case" );
      }
   }

   return result;
}

} // namespace

namespace json
{
JsonValueObject serializeFromFile(const std::string& fileName)
{
   std::ifstream ifs(fileName);
   Json::CharReaderBuilder builder;
   Json::Value documentRoot;
   builder["collectComments"] = false;
   Json::String errs;
   Json::parseFromStream(builder, ifs, &documentRoot, &errs);
   JsonValueObject root;

   for(auto it = documentRoot.begin(); it != documentRoot.end(); ++it)
   {
      if (isSingleElement(*it))
      {
         root.insert(it.name(), parseSingleElement(it));
      }
      else if (isArray(*it))
      {
         root.insert(it.name(), parseArray(*it));
      }
      else if (isObject(*it))
      {
         root.insert(it.name(), parseObject(*it));
      }
   }

   return root;
}
} // namespace json
