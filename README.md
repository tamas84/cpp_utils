# CPP Utils #


### What is this repository for? ###

This repository contains small tools for C++ projects.

### How do I get set up? ###

TBD
* Building the unit tests is enabled by default, can be disabled though
* To use these libs, just make sure you compile them and add them as dependencies (target_link_libraries)

### Who do I talk to? ###

* Gergely, Tamas  tamas.official7@gmail.com

### Table of contents ###

* auto_observer - 
A header only library that implements the Observer pattern
where it is not necessary for the Observers to unregister from
the Observable before their life time ends.
Because of the cost of the weak_ptr.lock() it is not advised to use this 
observer pattern implementation if the
notification of the Observers are likely to happen quite often. 
* circular_buffer -
A circular buffer solution with placement new, this means that the container
can be used with any T type, the type does not have to have a default ctor,
the ctor is not called N (capacity) times when the container is constructed.
TODO: not well tested, random access iterator should be used, etc.
* delegate -
A fast delegate implementation that does not use heap allocation, type erasure.
* free_memory_pool
A memory pool where any number of bytes memory can be allocated. No allocation information
is stored so an allocation can not be deallocated one by one, instead every allocation has
to be allocated at once (e.g. at the end of the main loop, etc). Complexity to get the
next free space is O(1). Not thread safe. TBD: memory alignment.
* jsonvalue -
A type which represents a JSON value. Can parse any JSON file into a 
map(like) structure.
Based on the std::variant, depends on jsoncpp.
* math -
    * angle -
A lightweight class that holds an angle in radians.
* random -
Wrapper classes around std::mt19937::result_type.
A deterministic and a regular one.
DeterministicRandom: when two instances initialized with the same value
then they will produce the same 'random' values
* realtime_logger -
A data logger which can be used to save the data into a .csv file, which can
be viewed with your favourite "Calc Spreadsheet" application to create 
helper Diagrams.
This logger is useful if the same variables needed to be logged in every 
iteration.
* safe_resource -
Encapsulates a mutex and data together to easily express what resources have to be
protected.
* sync_resource -
Encapsulates a mutex and data together to easily express what resources have to be
protected. Both mutual and shared exclusion is supported.
* thread_pool -
A reuseable thread pool where processing of each task is started immediately if possible.
std::future is used to indicate that the tasks are all processed.
* type_creator -
A global factory for different types. Elements are stored in a map where the
key is a base type and the value is a creator function.
* view -
A mutable or immutable view for a type T. Basically a wrapper that holds a reference to T.
It is possible to make a const view from any (const or non-const) view.
* detail - Things in detail are meant to be used from other cpp_unit subprojects.
    * log_callback -
A mechanism for the classes of cpp_unit to be able to `log` using the `client`'s logger.
So the client can specify the way cpp_unit tools should log.
* scope_guard - Pass a callable to it and that will be called when
execution leaves the scope.
