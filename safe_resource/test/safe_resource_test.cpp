#include <gtest/gtest.h>
#include <safe_resource.h>

#include <vector>


namespace
{

using namespace utils;

struct TestType
{
   std::string foo() const { return "text"; }
};

template <typename T>
void access(SafeResource<T>* resource)
{
   (*resource)([](T& i) {
      ++i;
      --i;
   });
}

class SafeResourceFixture : public ::testing::Test
{
public:
   SafeResourceFixture()
   {
   }

protected:
   SafeResource<int> sr_int = SafeResource<int>(5);
};

TEST_F(SafeResourceFixture, test_run)
{
   auto resource = sr_int.getResource();
   EXPECT_EQ(5, resource);
   auto func = [](int& i) {
      ++i;
   };
   sr_int(func);
   resource = sr_int.getResource();
   EXPECT_EQ(6, resource);
   resource = sr_int();
   EXPECT_EQ(6, resource);

   auto resultFunc = [](const int& i) {
      return i;
   };
   EXPECT_EQ(6, sr_int(resultFunc));
}

TEST_F(SafeResourceFixture, test_try_run)
{
   auto resource = sr_int.getResource();
   EXPECT_EQ(5, resource);
   auto func = [](int& i) -> int {
      return ++i;
   };
   auto expected = sr_int.tryRun(func);
   EXPECT_TRUE(expected.has_value());
   resource = expected.value();
   EXPECT_EQ(6, resource);
   resource = sr_int.getResource();
   EXPECT_EQ(6, resource);
   resource = sr_int();
   EXPECT_EQ(6, resource);
}

TEST_F(SafeResourceFixture, test_multiple_access)
{
   std::vector<std::thread> threads;
   threads.reserve(100);
   for (int i = 0; i < 100; ++i)
   {
      threads.emplace_back(access<int>, &sr_int);
   }

   for (auto& thread : threads)
   {
      thread.join();
   }

   EXPECT_EQ(5, sr_int());
   EXPECT_EQ(5, sr_int.getResource());
}


TEST_F(SafeResourceFixture, test_struct_as_T)
{
   SafeResource<TestType> sr = SafeResource<TestType>();
   auto& resource = sr.getResource();
   EXPECT_EQ("text", resource.foo());
}

} // namespace
