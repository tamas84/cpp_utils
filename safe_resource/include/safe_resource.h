#include <expected>
#include <mutex>


namespace utils
{

template <typename TValue>
class SafeResource
{
public:
   SafeResource()
      : m_resource(TValue{})
   {}

   explicit SafeResource(TValue v)
      : m_resource(std::move(v))
   {}

   template <typename Func, typename... Args>
   auto operator()(Func&& f, Args&& ...args)
   {
      std::lock_guard<std::mutex> _{ m_mutex };
      return f(m_resource, std::forward<Args>(args)...);
   }

   template <typename Func, typename... Args>
   auto tryRun(Func&& func, [[maybe_unused]] Args&& ...args) -> 
      std::expected<decltype(std::declval<decltype(func)>()(std::declval<TValue&>(), std::declval<Args>()...)),
                    bool>
   {
      std::unique_lock<std::mutex> lock(m_mutex, std::defer_lock);
      if (lock.try_lock())
      {
         return func(m_resource, std::forward<Args>(args)...);
      }
      return std::unexpected(bool{});
   }

   const TValue& operator()() const
   {
      return m_resource;
   }

   const TValue& getResource() const
   {
      return (*this)();
   }

   SafeResource(const SafeResource&) = delete;
   SafeResource(SafeResource&&) = delete;
   SafeResource& operator=(const SafeResource&) = delete;
   SafeResource& operator=(SafeResource&&) = delete;

private:
   mutable std::mutex m_mutex;
   TValue m_resource;
};

} // namespace utils
