#include <gtest/gtest.h>

#include <auto_life_time_observable.h>
#include <i_auto_life_time_observer.h>

#include <atomic>


namespace test
{

static std::atomic<int> global = 0;

struct MyType
{

};

struct Subject : public AutoLifeTimeObservable<MyType>
{

};

struct Observer : public IAutoLifeTimeObserver<MyType>
{
   void notify(const MyType&) override
   {
      global++;
   }
};



class AutoObserverTestFixture : public ::testing::Test
{
protected:
   virtual void SetUp()
   {
      global = 0;
   }

   virtual void TearDown()
   {
      global = 0;
   }

};

TEST_F(AutoObserverTestFixture, deleteTest)
{
   Subject subj;
   Observer* obs1 = new Observer();
   Observer* obs2 = new Observer();
   Observer* obs3 = new Observer();
   Observer* obs4 = new Observer();
   subj.registerObserver(*obs1);
   subj.registerObserver(*obs2);
   subj.registerObserver(*obs3);
   subj.registerObserver(*obs4);

   MyType type;
   subj.notifyObservers(type);
   EXPECT_EQ(4, global);

   delete obs3;
   delete obs4;
   subj.notifyObservers(type);
   EXPECT_EQ(6, global);

   delete obs1;
   delete obs2;
   subj.notifyObservers(type);
   EXPECT_EQ(6, global);

   subj.notifyObservers(type);
   EXPECT_EQ(6, global);
}

TEST_F(AutoObserverTestFixture, moveTest)
{
   Subject subj;
   Observer obs1;
   Observer obs2;
   Observer obs3;
   Observer obs4;
   subj.registerObserver(obs1);
   subj.registerObserver(obs2);
   subj.registerObserver(obs3);
   subj.registerObserver(obs4);

   MyType type;
   subj.notifyObservers(type);
   EXPECT_EQ(4, global);

   auto obsMoved3 = std::move(obs3);
   auto obsMoved4 = std::move(obs4);
   subj.notifyObservers(type);
   EXPECT_EQ(6, global);

   auto obsMoved1 = std::move(obs1);
   auto obsMoved2 = std::move(obs2);
   subj.notifyObservers(type);
   EXPECT_EQ(6, global);

   subj.notifyObservers(type);
   EXPECT_EQ(6, global);
}

} // namespace test
