#ifndef I_AUTO_LIFE_TIME_OBSERVABLE_H
#define I_AUTO_LIFE_TIME_OBSERVABLE_H

template <typename T>
class IAutoLifeTimeObserver;

template <typename T>
class IAutoLifeTimeObservable
{
public:
   virtual ~ IAutoLifeTimeObservable() = default;
   virtual void registerObserver(IAutoLifeTimeObserver<T>& observer) = 0;
   virtual void notifyObservers(const T& data) = 0;
};

#endif // I_AUTO_LIFE_TIME_OBSERVABLE_H
