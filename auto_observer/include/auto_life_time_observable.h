#ifndef AUTO_LIFE_TIME_OBSERVABLE_H
#define AUTO_LIFE_TIME_OBSERVABLE_H

#include "i_auto_life_time_observable.h"
#include "i_auto_life_time_observer.h"

#include <iostream>
#include <memory>
#include <mutex>
#include <vector>
#include <functional>

template <typename T>
class AutoLifeTimeObservable : public IAutoLifeTimeObservable<T>
{
private:
   struct ObserverAvailabilty
   {
      std::reference_wrapper <IAutoLifeTimeObserver<T>> observer;
      std::weak_ptr<uint8_t> existGuard;
   };

public:
   void registerObserver(IAutoLifeTimeObserver<T>& observer) override
   {
      if (observer.m_existGuard)
      {
         std::lock_guard<std::mutex> guard(m_observersMutex);
         ObserverAvailabilty obsA { std::ref(observer), observer.m_existGuard };
         m_observerWrappers.push_back (obsA);
      }
      else
      {
         std::cout << "Observer is already over of it's lifetime, so it is not added to list of observers\n";
      }
   }

   void notifyObservers(const T& data) override
   {
      std::lock_guard<std::mutex> guard(m_observersMutex);
      auto it = m_observerWrappers.begin();
      while (it != m_observerWrappers.end())
      {
         if (it->existGuard.lock())
         {
            it->observer.get().notify(data);
            ++it;
         }
         else
         {
            it = m_observerWrappers.erase(it);
            std::cout << "Observer expired, removing from the list of observers" << std::endl;
         }
      }
   }

   std::mutex m_observersMutex;
   std::vector <ObserverAvailabilty> m_observerWrappers;
};

#endif // AUTO_LIFE_TIME_OBSERVABLE_H
