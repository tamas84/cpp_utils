#ifndef I_AUTO_LIFE_TIME_OBSERVER_H
#define I_AUTO_LIFE_TIME_OBSERVER_H

#include <memory>

template <typename T>
class AutoLifeTimeObservable;

template <typename T>
class IAutoLifeTimeObserver
{
   friend class AutoLifeTimeObservable<T>;

public:
   IAutoLifeTimeObserver() = default;

   IAutoLifeTimeObserver(const IAutoLifeTimeObserver&)
   {
      m_existGuard = std::make_shared<uint8_t>(1);
   }

   IAutoLifeTimeObserver& operator=(const IAutoLifeTimeObserver& o)
   {
      if (this != *o)
      {
         m_existGuard = std::make_shared<uint8_t>(1);
      }
      return *this;
   }

   IAutoLifeTimeObserver(IAutoLifeTimeObserver&& o)
   {
      m_existGuard = std::make_shared<uint8_t>(1);
      o.m_existGuard = nullptr;
   }

   IAutoLifeTimeObserver& operator=(IAutoLifeTimeObserver&& o)
   {
      if (this != *o)
      {
         m_existGuard = std::make_shared<uint8_t>(1);
      }
      return *this;
   }

   virtual ~IAutoLifeTimeObserver() = default;

   virtual void notify(const T& data) = 0;

private:
   std::shared_ptr<uint8_t> m_existGuard = std::make_shared<uint8_t>(1);
};

#endif // I_AUTO_LIFE_TIME_OBSERVER_H
